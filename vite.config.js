import { defineConfig } from "vite";
import { VitePWA } from 'vite-plugin-pwa'
import react from "@vitejs/plugin-react";
import tailwindcss from "tailwindcss";

const manifestForPlugin = {
    registerType:'prompt',
    includeAssets:['favicon.ico', "apple-touch-icon.png", "masked-icon.svg"],
    manifest:{
      name:"XU CMO - ECMS",
      short_name:"XU - ECMS",
      description:"Official Certificate Management System of XU CMO",
      icons:[
        {
            "src": "/pwa-192x192.png",
            "sizes": "192x192",
            "type": "image/png",
            "purpose": "any"
          },
          {
            "src": "/pwa-512x512.png",
            "sizes": "512x512",
            "type": "image/png",
            "purpose": "any"
          },
          {
            "src": "/pwa-maskable-192x192.png",
            "sizes": "192x192",
            "type": "image/png",
            "purpose": "maskable"
          },
          {
            "src": "/pwa-maskable-512x512.png",
            "sizes": "512x512",
            "type": "image/png",
            "purpose": "maskable"
          }
    ],
    theme_color:'#181818',
    background_color:'#e0cc3b',
    display:"standalone",
    scope:'/',
    start_url:"/",
    orientation:'portrait'
    },
  };

export default defineConfig({
    plugins:[
        react(),
        VitePWA(manifestForPlugin)
    ],
    css: {
        postcss: {
            plugins:[tailwindcss()],
        },
    }
})