import React, { useState, useEffect, useRef } from 'react';

import { useQuery, useQueryClient } from '@tanstack/react-query';
import axiosConfig from '../../axiosConfig'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faSearch, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';

import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import CircularProgress from '@mui/material/CircularProgress';

import { DateRangePicker } from 'rsuite';

import Sidebar from '../components/Sidebar';

import Swal from 'sweetalert2';

const eventDetails = {
    EventsList: {
      name: 'EventTypes',
      groups: [
        {
          label: 'General',
          types: ['Onsite event', 'Online event']
        },
        {
          label: 'Recollection',
          types: ['Onsite Recollection', 'Online Recollection']
        },
        {
          label: 'Retreat',
          types: ['Onsite Retreat', 'Online Retreat']
        },
      ]
    }
  };

const CertificateTemplates = () => {

  const bgImgInputRef = useRef(null);
  const sigImgInputRef = useRef(null);

  const queryClient = useQueryClient();

  const [isSidebarOpen, setIsSidebarOpen] = useState(true);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalMode, setModalMode] = useState('add'); // 'add' or 'edit'
  const [dateRange, setDateRange] = useState([new Date(), new Date()]);
  const [isDateRangeError, setIsDateRangeError] = useState(false);
  const [currentTemplate, setCurrentTemplate] = useState(null); // Template being edited
  const [formData, setFormData] = useState({
    templateTitle: '',
    certBgImgKey: '',
    certEventYearLevel: '',
    certEventType: '',
    certEventTheme: '',
    certEventDate: null,
    certEventVenue: '',
    certDirectorName: '',
    certSigImgKey: ''
});

const [searchKeyword, setSearchKeyword] = useState('');
const [debouncedSearchKeyword, setDebouncedSearchKeyword] = useState(searchKeyword);

    // Debounce searchKeyword
    useEffect(() => {
        const handler = setTimeout(() => {
            setDebouncedSearchKeyword(searchKeyword);
        }, 500); // 500ms delay for debouncing

        return () => {
            clearTimeout(handler);
        };
    }, [searchKeyword]);

const handleSearch = () => {
    const keyword = event.target.value;
    setSearchKeyword(keyword);
};

    // Retrieve the token from localStorage
    const token = localStorage.getItem('token');

    const { data: templates, isLoading } = useQuery({
        queryKey: ['templates', debouncedSearchKeyword],
        queryFn: async () => {
            if (!token) {
                throw new Error("Authorization token is not available.");
            }

            const config = {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            };

            // Fetch data from the backend with search keyword and auth headers
            const response = await axiosConfig.get(`https://e-cms-capstone-backend-hazel.vercel.app/certificate-templates/templates-list?keyword=${encodeURIComponent(debouncedSearchKeyword)}`, config);
            return response.data.data;
        },
        enabled: !!token // Only run the query if the token exists
    });

    const [certBgImg, setCertBgImg] = useState(null);
    const [certSigImg, setCertSigImg] = useState(null);

    const handleBgImageChange = (event) => {
        const file = event.target.files[0];
        if (!file) {
            Swal.fire('Error', 'No file selected.', 'error');
            return;
        }
    
        // Only accept JPEG and PNG files
        if (!file.type.match('image/jpeg') && !file.type.match('image/png')) {
            Swal.fire('Invalid File Type', 'Please upload a JPEG or PNG image file.', 'error');
            event.target.value = ''; // Reset the input
            return;
        }
    
        if (file.size > 2097152) { // 2MB in bytes
            Swal.fire('File Too Large', 'Please upload a file smaller than 2MB.', 'error');
            event.target.value = ''; // Reset the input
            return;
        }
    
        // Trim the file name and remove special characters
        const newFileName = file.name.trim().replace(/[^a-zA-Z0-9_.-]/g, '_');
        const newFile = new File([file], newFileName, { type: file.type });
    
        setCertBgImg(newFile);
    };
    
    const handleSigImageChange = (event) => {
        const file = event.target.files[0];
        if (!file) {
            Swal.fire('Error', 'No file selected.', 'error');
            return;
        }
    
        // Only accept JPEG and PNG files
        if (!file.type.match('image/jpeg') && !file.type.match('image/png')) {
            Swal.fire('Invalid File Type', 'Please upload a JPEG or PNG image file.', 'error');
            event.target.value = ''; // Reset the input
            return;
        }
    
        if (file.size > 2097152) { // 2MB in bytes
            Swal.fire('File Too Large', 'Please upload a file smaller than 2MB.', 'error');
            event.target.value = ''; // Reset the input
            return;
        }
    
        // Trim the file name and remove special characters
        const newFileName = file.name.trim().replace(/[^a-zA-Z0-9_.-]/g, '_');
        const newFile = new File([file], newFileName, { type: file.type });
    
        setCertSigImg(newFile);
    };

  const toggleSidebar = () => setIsSidebarOpen(!isSidebarOpen);


    // Modify handleTemplateClick for setting view mode
    const handleTemplateClick = (template) => {
        setCurrentTemplate(template);
        setModalMode('view'); // Set mode to 'view' when a template is clicked
        setIsModalOpen(true);
    };

    const handleTemplateEdit = (template) => {
        setCurrentTemplate(template);
        setModalMode('edit');

        // Assuming certEventDate in the template can be a single date or a 'date to date' format
        const dateValue = template.certEventDate;
        let dates = dateValue.split(' - ').map(date => new Date(date.trim()));
        if (dates.length === 1) {
            dates = [dates[0], dates[0]]; // Ensure it's a range even for single dates
        }

        setDateRange(dates);
    
        // Set initial form data with existing template details
        setFormData({
            templateTitle: template.templateTitle,
            certEventYearLevel: template.certEventYearLevel,
            certEventType: template.certEventType,
            certEventTheme: template.certEventTheme,
            certEventDate: dateValue,
            certEventVenue: template.certEventVenue,
            certDirectorName: template.certDirectorName
        });
    
        // Initialize file states to null since we are editing
        setCertBgImg(null);
        setCertSigImg(null);
    
        setIsModalOpen(true);
    };

    const deleteTemplate = async (templateId) => {
        const confirmed = await Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        });
    
        if (confirmed.isConfirmed) {

            Swal.fire({
                title: 'Please wait...',
                text: 'Processing your request.',
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading();
                }
            });

            try {
                const response = await axiosConfig.delete(`https://e-cms-capstone-backend-hazel.vercel.app/certificate-templates/delete-template/${templateId}`, {
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                });
    
                if (response.status === 200) {
                    Swal.fire(
                        'Deleted!',
                        'Template has been deleted successfully.',
                        'success'
                    );
                    // Invalidate and refetch the templates list to update the UI
                    queryClient.invalidateQueries(['templates']);
                }
            } catch (error) {
                console.error('Failed to delete template:', error);
                Swal.fire(
                    'Failed!',
                    'Failed to delete template: ' + (error.response?.data?.message || 'Server error'),
                    'error'
                );
            }
        }
    };

  const handleAddTemplate = () => {
    setModalMode('add');
    setCurrentTemplate(null);
    setDateRange([new Date(), new Date()]);
    setFormData({
        templateTitle: '',
        certBgImgKey: '',
        certEventYearLevel: '',
        certEventType: '',
        certEventTheme: '',
        certEventDate: null,
        certEventVenue: '',
        certDirectorName: '',
        certSigImgKey: ''
    });
    setIsModalOpen(true);
};

const handleSubmit = async (event) => {
    event.preventDefault();

    // Validation checks for other form fields
    if (!formData.templateTitle || !formData.certEventYearLevel || !formData.certEventType || !formData.certEventTheme || !formData.certEventDate || !formData.certEventVenue || !formData.certDirectorName) {
        Swal.fire('Error', 'Please fill in all required fields.', 'error');
        return;
    }

    const formDataToSend = new FormData();
    Object.keys(formData).forEach(key => {
        formDataToSend.append(key, formData[key]);
    });

    // Append files only if they have been updated
    if (certBgImg) {
        formDataToSend.append('certBgImg', certBgImg);
    } else if (modalMode === 'edit' && currentTemplate.certBgImgKey) {
        formDataToSend.append('certBgImgKey', currentTemplate.certBgImgKey);
    }

    if (certSigImg) {
        formDataToSend.append('certSigImg', certSigImg);
    } else if (modalMode === 'edit' && currentTemplate.certSigImgKey) {
        formDataToSend.append('certSigImgKey', currentTemplate.certSigImgKey);
    }

    Swal.fire({
        title: 'Please wait...',
        text: 'Processing your request.',
        allowOutsideClick: false,
        didOpen: () => {
            Swal.showLoading();
        }
    });

    try {
        let url = 'https://e-cms-capstone-backend-hazel.vercel.app/certificate-templates/generate-template';
        let method = 'post';
        
        if (modalMode === 'edit' && currentTemplate) {
            url = `https://e-cms-capstone-backend-hazel.vercel.app/certificate-templates/update-template/${currentTemplate._id}`;
            method = 'put';
        }

        const response = await axiosConfig({
            method: method,
            url: url,
            data: formDataToSend,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'multipart/form-data'
            }
        });

        Swal.close();
        if (response.status === 200 || response.status === 201) {
            const successMessage = modalMode === 'add' ? 'Template created successfully.' : 'Template updated successfully.';
            Swal.fire('Success!', successMessage, 'success');
            setIsModalOpen(false);
            queryClient.invalidateQueries(['templates']);
        }
    } catch (error) {
        Swal.close();
        console.error(`Error ${modalMode === 'add' ? 'creating' : 'updating'} template:`, error);
        Swal.fire('Error!', error.response?.data?.message || `An error occurred while ${modalMode === 'add' ? 'creating' : 'updating'} the template.`, 'error');
    }
};

const handleClose = () => {
    // console.log('Closing modal and resetting form data');
    setIsModalOpen(false);
    // Reset form data when modal closes
    setFormData({
        templateTitle: '',
        certBgImgKey: '',
        certEventYearLevel: '',
        certEventType: '',
        certEventTheme: '',
        certEventDate: '',
        certEventVenue: '',
        certDirectorName: '',
        certSigImgKey: ''
    });
};

const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData(prevFormData => ({ ...prevFormData, [name]: value }));
};

        // Modal style
        const style = {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            bgcolor: 'background.paper',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4,
        };

// Dynamically set the modal title
const modalTitle = modalMode === 'add' ? 'Add Template' : modalMode === 'edit' ? 'Edit Template' : 'View Template';

// Define the modal content for view mode
const renderViewModeContent = () => (
    <Box sx={style}>
      <h2 id="modal-modal-title" className="text-lg font-bold mb-4">
        View Template: {currentTemplate.templateTitle}
      </h2>
      <div className="space-y-4">
        <p><strong>Title:</strong> {currentTemplate.templateTitle}</p>
        <p><strong>Background Image Link:</strong> {currentTemplate.certBgImgKey}</p>
        <p><strong>Event Year Level:</strong> {currentTemplate.certEventYearLevel}</p>
        <p><strong>Event Type:</strong> {currentTemplate.certEventType}</p>
        <p><strong>Event Theme:</strong> {currentTemplate.certEventTheme}</p>
        <p><strong>Event Date:</strong> {currentTemplate.certEventDate}</p>
        <p><strong>Event Venue:</strong> {currentTemplate.certEventVenue}</p>
        <p><strong>Director Name:</strong> {currentTemplate.certDirectorName}</p>
        <p><strong>Signature Image Link:</strong> {currentTemplate.certSigImgKey}</p>
      </div>
      <div className="mt-4">
        <button type="button" onClick={handleClose} className="px-4 py-2 bg-red-500 text-white rounded hover:bg-red-600">
          Close
        </button>
      </div>
    </Box>
  );


  return (

    <>
        <Sidebar isCollapsed={!isSidebarOpen} toggleSidebar={toggleSidebar} />

            <div className={`bg-[#edf0f7] min-h-screen transition-padding duration-300 ${isSidebarOpen ? 'pl-64' : 'pl-16'}`}>
            <h1 className="text-4xl font-semibold text-[#3a53a5] bg-[#D9D9D9] text-center p-3">CERTIFICATE TEMPLATES</h1>

            <div className="p-5 bg-[#edf0f7] min-h-screen">
                <div className="bg-white p-5 rounded-lg shadow-md">
                    <div className="mb-5 flex justify-between items-center">
                    <button onClick={handleAddTemplate} className="bg-blue-500 hover:bg-blue-600 text-white py-2.5 px-5 rounded-md border-0 cursor-pointer">
                        <FontAwesomeIcon icon={faPlus} /> Add Template
                    </button>
                        <div className="flex items-center mr-4">
                        <FontAwesomeIcon icon={faSearch} className="text-gray-400 mr-2" />
                        <TextField
                            className="p-2.5 rounded-md border border-gray-300 w-48"
                            id="search-input"
                            label="Search"
                            variant="outlined"
                            size="small"
                            value={searchKeyword}
                            onChange={handleSearch}
                        />
                        </div>
                    </div>

                    <Modal open={isModalOpen} onClose={handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description" sx={{ zIndex: 20 }}>
                        <Box sx={style}>
                            <h2 id="modal-modal-title" className="text-lg font-bold mb-4">
                                {modalTitle}
                            </h2>
                            {
                        modalMode === 'view' ? renderViewModeContent() : (
                            <form onSubmit={handleSubmit}>

                            {/* Template Title */}

                            <label htmlFor="templateTitle" className="block text-sm font-medium text-gray-700">
                                Title
                            </label>

                            <input
                                name="templateTitle"
                                type="text"
                                placeholder="Enter Template Title"
                                value={formData.templateTitle}
                                onChange={handleChange}
                                className="block w-full mb-3 px-3 py-1.5 border rounded"
                            />

                            {/* Certificate Background Image Upload */}

                            <label htmlFor="certBgImgKey" className="block text-sm font-medium text-gray-700">
                                Certificate Background Image Upload
                            </label>

                            <input
                                ref={bgImgInputRef}
                                name="certBgImgKey"
                                type="file"
                                accept="image/*"
                                onChange={handleBgImageChange}
                                className="block w-full mb-3 px-3 py-1.5 border rounded"
                            />

                            {/* Event Year Level Dropdown */}

                            <label htmlFor="certEventYearLevel" className="block text-sm font-medium text-gray-700">
                                Year Level
                            </label>

                            <select
                                name="certEventYearLevel"
                                value={formData.certEventYearLevel}
                                onChange={handleChange}
                                className="block w-full mb-3 px-3 py-2 border rounded"
                            >
                                <option value="">Select Year Level</option>
                                <option value="1st">1st</option>
                                <option value="2nd">2nd</option>
                                <option value="3rd">3rd</option>
                                <option value="4th">4th</option>
                            </select>

                            {/* Event Type */}

                            <label htmlFor="certEventType" className="block text-sm font-medium text-gray-700">
                                Event Type
                            </label>

                            <select
                            name="certEventType"
                            value={formData.certEventType}
                            onChange={handleChange}
                            className="block w-full mb-3 px-3 py-2 border rounded"
                            required
                            >
                            <option value="">Select Event Type</option>
                            {eventDetails.EventsList.groups.map((group) => (
                                <optgroup key={group.label} label={group.label}>
                                {group.types.map((type) => (
                                    <option key={type} value={type}>{type}</option>
                                ))}
                                </optgroup>
                            ))}
                            </select>

                            {/* Event Theme */}

                            <label htmlFor="certEventTheme" className="block text-sm font-medium text-gray-700">
                                Event Theme
                            </label>

                            <input
                                name="certEventTheme"
                                type="text"
                                placeholder="Enter Event Theme"
                                value={formData.certEventTheme}
                                onChange={handleChange}
                                className="block w-full mb-3 px-3 py-1.5 border rounded"
                            />

                            {/* Event Date */}

                            <label htmlFor="certEventDate" className="block text-sm font-medium text-gray-700">
                                Event Date
                            </label>
                            
                            <DateRangePicker
                                className={'w-full'}
                                placeholder="Select Date or Range"
                                value={dateRange}
                                onChange={(range) => {
                                    setDateRange(range);
                                    if (!range) {
                                        setFormData(prevFormData => ({
                                            ...prevFormData,
                                            certEventDate: ''
                                        }));
                                        setIsDateRangeError(true);
                                    } else if (range.length === 2 && range[0] && range[1]) {
                                        let startDate = range[0].toLocaleDateString();
                                        let endDate = range[1].toLocaleDateString();
                                        let dateValue = startDate === endDate ? startDate : `${startDate} - ${endDate}`;
                                        setFormData(prevFormData => ({
                                            ...prevFormData,
                                            certEventDate: dateValue
                                        }));
                                        setIsDateRangeError(false);
                                    }
                                }}
                            />

                            {/* Event Venue */}

                            <label htmlFor="certEventVenue" className="block text-sm font-medium text-gray-700 mt-3">
                                Event Venue
                            </label>

                            <input
                                name="certEventVenue"
                                type="text"
                                placeholder="Enter Event Venue"
                                value={formData.certEventVenue}
                                onChange={handleChange}
                                className="block w-full mb-3 px-3 py-1.5 border rounded"
                            />

                            {/* Director Name */}


                            <label htmlFor="certDirectorName" className="block text-sm font-medium text-gray-700">
                                Director Name
                            </label>

                            <input
                                name="certDirectorName"
                                type="text"
                                placeholder="Enter Director Name"
                                value={formData.certDirectorName}
                                onChange={handleChange}
                                className="block w-full mb-3 px-3 py-1.5 border rounded"
                            />

                            {/* Director Signature Image Upload */}

                            <label htmlFor="certSigImgKey" className="block text-sm font-medium text-gray-700">
                                Director Signature Image Upload
                            </label>

                            <input
                                ref={sigImgInputRef}
                                name="certSigImgKey"
                                type="file"
                                accept="image/*"
                                onChange={handleSigImageChange}
                                className="block w-full mb-3 px-3 py-1.5 border rounded"
                            />

                            {/* Action Buttons */}
                            <button type="button" onClick={handleClose} className="px-4 py-2 bg-red-500 text-white rounded hover:bg-red-600">Cancel</button>
                            <button type="submit" className="ml-2 px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600">
                                {modalMode === 'add' ? 'Add Template' : 'Update Template'}
                            </button>
                        </form>
                        )
                    }
                        </Box>
                    </Modal>
                    {isLoading?
                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '25vh', backgroundColor: 'rgba(255, 255, 255, 0.5)' }}>
                    <CircularProgress style={{ color: 'blue' }} />
                    </div> :
                    <div className="rounded overflow-y-auto max-h-[600px] bg-[#D9D9D9]">
                            <div className="grid grid-cols-3 gap-5 p-5">
                                {templates.map((template) => (
                                    <div
                                        key={template._id} // Unique identifier
                                        style={{ backgroundImage: `url(${template.certBgImgKey})`, backgroundSize: 'cover' }}
                                        className="group border border-blue-600 rounded-md p-20 relative bg-white hover:bg-blue-50 transition duration-300"
                                        onClick={() => handleTemplateClick(template)} // Card-level click handler
                                    >
                                        <div className="mb-2.5 flex justify-end items-center space-x-2 absolute top-2 right-2 z-10 opacity-0 group-hover:opacity-100 transition-opacity duration-300">
                                            <button onClick={(e) => {
                                                e.stopPropagation(); // Stop click event from bubbling up to the card
                                                handleTemplateEdit(template);
                                            }}>
                                                <FontAwesomeIcon icon={faEdit} className="cursor-pointer" />
                                            </button>
                                            <button onClick={(e) => {
                                                e.stopPropagation(); // Stop click event from bubbling up to the card
                                                deleteTemplate(template._id);
                                            }}>
                                                <FontAwesomeIcon icon={faTrash} className="cursor-pointer" />
                                            </button>
                                        </div>
                                        <div 
                                            style={{
                                                position: 'absolute',
                                                top: 0,
                                                left: 0,
                                                right: 0,
                                                bottom: 0,
                                                backgroundColor: 'rgba(255, 255, 255, 0.5)', // 50% transparent white overlay
                                                display: 'flex',
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                                color: 'black' // Ensure text color contrasts with light overlay
                                            }}
                                        >
                                            <h3 className="p-4 font-bold text-sm">{template.templateTitle}</h3>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                        }
                    </div>
                </div>
            </div>
    </>
  );
};

export default CertificateTemplates;