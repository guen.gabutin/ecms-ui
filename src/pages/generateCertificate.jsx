import React, {useState, useEffect} from 'react';
import { useMailData } from '../components/contexts/MailDataContext';
import Sidebar from '../components/Sidebar';
import Generate from '../components/Generate';
import Solo from '../components/Solo';
import Batch from '../components/Batch';
import Mail from '../components/Mail';

import useRollback from '../components/hooks/useRollback';

const GenerateCertificate = () => {
  const { activeTab, setActiveTab, mailData, batchId, processCompleted, resetContext } = useMailData();
  const [isSidebarOpen, setIsSidebarOpen] = useState(true);
  const rollback = useRollback();


  const toggleSidebar = () => setIsSidebarOpen(!isSidebarOpen);

  useEffect(() => {
    // Call rollback on mount to reset any previous state
    rollback(batchId, processCompleted);
    resetContext();
    // Additional setup or cleanup can be added here
  }, []); // Empty dependency array ensures this runs only on mount

  return (
    <>
      <Sidebar isCollapsed={!isSidebarOpen} toggleSidebar={toggleSidebar} />
      <div className={`bg-[#edf0f7] min-h-screen transition-padding duration-300 ${isSidebarOpen ? 'pl-64' : 'pl-16'}`}>
        <h1 className="text-4xl font-semibold text-[#3a53a5] bg-[#D9D9D9] text-center p-3">GENERATE CERTIFICATE</h1>
        <div className="flex p-20 justify-center items-center h-screen bg-gray-100">
          <div className="bg-white p-8 rounded-xl shadow-xl w-full max-w-3xl">
            {activeTab === 'generate' && (<Generate navigateToBatch={() => setActiveTab('batch')} />)}
            {activeTab === 'mail' && (<Mail data={mailData} />)}
            {activeTab === 'solo' && ( <Solo />)}
            {activeTab === 'batch' && ( <Batch />)}
          </div>
        </div>
      </div>
    </>
  );
};

export default GenerateCertificate;