import React, { useState, useEffect }from 'react';
import Sidebar from '../components/Sidebar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faSearch, faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import { DataGrid } from '@mui/x-data-grid';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import CircularProgress from '@mui/material/CircularProgress';

import { useQuery, useQueryClient } from '@tanstack/react-query';
import axiosConfig from '../../axiosConfig'

import Swal from 'sweetalert2';


function AccountsCenter() {
    const [isSidebarOpen, setIsSidebarOpen] = useState(true);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [modalMode, setModalMode] = useState('add'); // 'add' or 'edit'
    const [currentUser, setCurrentUser] = useState(null); // User being edited
    const [formData, setFormData] = useState({
        email: '',
        username: '',
        password: '',
        confirmPassword: '',
        role: '',
    });

    const toggleSidebar = () => {
      setIsSidebarOpen(!isSidebarOpen);
    };

    const queryClient = useQueryClient();

    const handleClose = () => {
        // console.log('Closing modal and resetting form data');
        setIsModalOpen(false);
        // Reset form data when modal closes
        setFormData({
            email: '',
            username: '',
            password: '',
            confirmPassword: '',
            role: '',
        });
    };

    const handleChange = (event) => {
        const { name, value } = event.target;
        setFormData(prevFormData => ({ ...prevFormData, [name]: value }));
    };

        // Modal style
        const style = {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            bgcolor: 'background.paper',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4,
        };

    const [searchKeyword, setSearchKeyword] = useState('');
    const [debouncedSearchKeyword, setDebouncedSearchKeyword] = useState(searchKeyword);

        // Debounce searchKeyword
        useEffect(() => {
            const handler = setTimeout(() => {
                setDebouncedSearchKeyword(searchKeyword);
            }, 500); // 500ms delay for debouncing
    
            return () => {
                clearTimeout(handler);
            };
        }, [searchKeyword]);

    const handleSearch = () => {
        const keyword = event.target.value;
        setSearchKeyword(keyword);
        setPaginationModel({ page: 0, pageSize: paginationModel.pageSize });
      };

        // Columns for Material UI Data Table
        const columns = [
            // { field: 'id', headerName: 'ID', width: 90 },
            { field: 'username', headerName: 'Username', width: 150 },
            { field: 'email', headerName: 'Email', width: 200 },
            { field: 'role', headerName: 'Role', width: 130 },
            {
                field: 'actions',
                headerName: 'Actions',
                sortable: false,
                width: 150,
                renderCell: (params) => {
                    const handleEdit = () => {
                        handleEditUser(params.row);
                    };
                    const handleDelete = () => {
                        handleDeleteUser(params.id)
                    };
    
                    return (
                        <>
                            <button onClick={handleEdit} style={{ marginRight: '10px' }}>
                                <FontAwesomeIcon icon={faEdit} />
                            </button>
                            <button onClick={handleDelete}>
                                <FontAwesomeIcon icon={faTrashAlt} />
                            </button>
                        </>
                    );
                },
            },
        ];
    
        const [page] = useState(1);

        // Initialize pagination model state
        const [paginationModel, setPaginationModel] = useState({
            page: 0,
            pageSize: 5,
        });

        // Adjusting query to include dependency on page for automatic refetching
        const { data, error, isLoading } = useQuery({
            queryKey: ['users', paginationModel.page + 1, paginationModel.pageSize, debouncedSearchKeyword],
            queryFn: () =>
                axiosConfig
                    .get(`https://e-cms-capstone-backend-hazel.vercel.app/users/user-list?page=${paginationModel.page + 1}&limit=${paginationModel.pageSize}&keyword=${encodeURIComponent(debouncedSearchKeyword)}`)
                    .then(res => res.data),
            keepPreviousData: true,
        });

        if (error) return <div>An error occurred: {error.message}</div>;

        // console.log(data);
        

        const handleAddUser = () => {
            setModalMode('add');
            setCurrentUser(null);
            setFormData({
                email: '',
                username: '',
                password: '',
                confirmPassword: '',
                role: '',
            });
            setIsModalOpen(true);
        };
    
        const handleEditUser = (user) => {
            setModalMode('edit');
            setCurrentUser(user);
            setFormData({
                username: user.username,
                email: user.email,
                password: '', // Password should not be pre-filled
                confirmPassword: '',
                role: user.role,
            });
            setIsModalOpen(true);
        };

        const handleDeleteUser = async (userId) => {
            const token = localStorage.getItem('token');

            // Show a confirmation dialog before deletion
            const result = await Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            });
        
            // Proceed with deletion only if the user confirmed
            if (result.isConfirmed) {
                try {
                    const response = await axiosConfig.delete(`https://e-cms-capstone-backend-hazel.vercel.app/users/delete-user/${userId}`, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    });
                    
                    if (response.status === 200) {
                        Swal.fire(
                            'Deleted!',
                            'The user has been deleted.',
                            'success'
                        );
                        queryClient.invalidateQueries(['users']); // Refetch the user list
                    }
                } catch (error) {
                    console.error('Error deleting user:', error);
                    Swal.fire(
                        'Error!',
                        error.response?.data?.message || 'An error occurred while deleting the user.',
                        'error'
                    );
                }
            }
        };
    
        const handleSubmit = async (event) => {
            event.preventDefault();
            const token = localStorage.getItem('token');
        
        // Simple front-end validation for required fields
        if (!formData.username || !formData.email || (modalMode === 'add' && (!formData.password || !formData.confirmPassword))) {
            Swal.fire('Error', 'Please fill in all fields.', 'error');
            return;
        }

        // Username Validation
        const usernameRegex = /^[a-zA-Z0-9_-]+$/;
        if (!usernameRegex.test(formData.username)) {
            Swal.fire('Error', 'Username must not contain spaces or special characters except underscores (_) and dashes (-).', 'error');
            return;
        }

        // Check if email format is correct
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!emailRegex.test(formData.email)) {
            Swal.fire('Error', 'Please enter a valid email address.', 'error');
            return;
        }

        // Password validation for add mode or if password is being changed in edit mode
        if (modalMode === 'add' || (modalMode === 'edit' && formData.password.trim())) {
            const passwordRegex = /^(?=.*[A-Z])(?=.*[!@#$%^&*()_+{}:">?<;,-.])[a-zA-Z0-9!@#$%^&*()_+{}:">?<;,-.]{8,}$/;
            if (!passwordRegex.test(formData.password)) {
                Swal.fire('Error', 'Password must be at least 8 characters long, contain no spaces, include at least one uppercase letter, and one special character.', 'error');
                return;
            }

            // Check if passwords match
            if (formData.password !== formData.confirmPassword) {
                Swal.fire('Error', 'Passwords do not match.', 'error');
                return;
            }
        }
        
            if (modalMode === 'add') {
                // Construct the user data object for adding new users
                const userData = {
                    username: formData.username,
                    email: formData.email,
                    password: formData.password,
                    confirmPassword: formData.confirmPassword,
                    role: formData.role,
                };
        
                // Attempt to create a new user
                try {
                    const response = await axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/users/create-account', userData, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    });
        
                    // Check the response status code
                    if (response.status === 201) {
                        Swal.fire('Success!', 'User created successfully.', 'success');
                        setIsModalOpen(false); // Close the modal
                        queryClient.invalidateQueries(['users']); // Refetch the user list
                    }
                } catch (error) {
                    console.error('Error creating user:', error);
                    Swal.fire('Error!', error.response?.data?.message || 'An error occurred while creating the user.', 'error');
                }
            } else if (modalMode === 'edit' && currentUser) {
                // Prompt confirmation before updating
                const result = await Swal.fire({
                    title: 'Are you sure?',
                    text: "You are about to update the user's information.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, update it!',
                });
        
                if (result.isConfirmed) {
                    const updatedData = {
                        username: formData.username,
                        email: formData.email,
                        role: formData.role,
                    };

                    // console.log("Password:", formData.password);
                    // console.log("Confirm Password:", formData.confirmPassword);
        
                    // Include password fields only if they are filled
                    if (formData.password && formData.password.trim() !== '') {
                        if (formData.password !== formData.confirmPassword) {
                            Swal.fire('Error', 'Passwords do not match.', 'error');
                            return;
                        }
                        updatedData.password = formData.password;
                    }
        
                    try {
                        const response = await axiosConfig.put(`https://e-cms-capstone-backend-hazel.vercel.app/users/edit-user/${currentUser.id}`, updatedData, {
                            headers: {
                                'Authorization': `Bearer ${token}`
                            }
                        });
        
                        if (response.status === 200) {
                            Swal.fire('Updated!', 'The user has been updated.', 'success');
                            setIsModalOpen(false); // Close the modal
                            queryClient.invalidateQueries(['users']); // Refetch the user list
                        }
                    } catch (error) {
                        console.error('Error updating user:', error);
                        Swal.fire('Error!', error.response?.data?.message || 'An error occurred while updating the user.', 'error');
                    }
                }
            }
        };

        // Adjust how rows are set based on the new data structure
        const rows = data?.data
        ? data.data.map(user => ({
              id: user._id,
              username: user.username,
              email: user.email,
              role: user.role,
          }))
        : [];

        // console.log(rows);


  return (

    <>
    <Sidebar isCollapsed={!isSidebarOpen} toggleSidebar={toggleSidebar}/>

    <div>
        <div className={`bg-[#edf0f7] min-h-screen transition-padding duration-300 ${isSidebarOpen ? 'pl-64' : 'pl-16'}`}>
            <h1 className="text-4xl font-semibold text-[#3a53a5] bg-[#D9D9D9] mb-6 text-center p-3">MANAGE ACCOUNTS</h1>

            <div>
                <button onClick={handleAddUser} className="flex items-center ml-10 px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600">
                    <FontAwesomeIcon icon={faPlus} className="mr-2" />
                    Add User
                </button>
            </div>

            <Modal open={isModalOpen} onClose={handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description" sx={{ zIndex: 600}}>
                <Box sx={style}>
                    <h2 id="modal-modal-title" className="text-lg font-bold mb-4">{modalMode === 'add' ? 'Add New User' : 'Edit User'}</h2>
                    <form onSubmit={handleSubmit}>
                        <input
                            name="username"
                            type="text"
                            placeholder="Username"
                            value={formData.username}
                            onChange={handleChange}
                            className="block w-full mb-3 px-3 py-1.5 border rounded"
                        />
                        <input
                            name="email"
                            type="email"
                            placeholder="Email"
                            value={formData.email}
                            onChange={handleChange}
                            className="block w-full mb-3 px-3 py-1.5 border rounded"
                        />
                        { (modalMode === 'add' || modalMode === 'edit') && (
                            <>
                                <input
                                    name="password"
                                    type="password"
                                    placeholder="Password"
                                    value={formData.password.trim()}
                                    onChange={handleChange}
                                    className="block w-full mb-3 px-3 py-1.5 border rounded"
                                />
                                <input
                                    name="confirmPassword"
                                    type="password"
                                    placeholder="Confirm Password"
                                    value={formData.confirmPassword.trim()}
                                    onChange={handleChange}
                                    className="block w-full mb-3 px-3 py-1.5 border rounded"
                                />
                            </>
                        )}
                        <select
                            name="role"
                            value={formData.role}
                            onChange={handleChange}
                            className="block w-full mb-3 px-3 py-2 border rounded"
                        >
                            <option value="">Select User Role</option>
                            <option value="StudentAssistant">Student Assistant</option>
                            <option value="Staff">Staff</option>
                            <option value="Admin">Admin</option>
                        </select>
                        <button type="button" onClick={handleClose} className="px-4 py-2 bg-red-500 text-white rounded hover:bg-red-600">Cancel</button>
                        <button type="submit" className="ml-2 px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600">
                            {modalMode === 'add' ? 'Add User' : 'Update User'}
                        </button>
                    </form>
                </Box>
            </Modal>

            <section className="bg-white rounded-2xl shadow-lg m-9">
                <div className="px-6 py-4 flex justify-between items-center border-b">
                    <h2 className="text-xl font-semibold">System Accounts</h2>
                    <div className="flex items-center mr-4">
                        <FontAwesomeIcon icon={faSearch} className="text-gray-400 mr-2" />
                        <TextField 
                                id="search-input"
                                className="mr-4"
                                label="Search"
                                variant="outlined"
                                size="small"
                                value={searchKeyword}
                                onChange={handleSearch} />
                    </div>
                </div>

                <div style={{ height: 400, width: '100%' }}>
                    {isLoading? 
                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '25vh', backgroundColor: 'rgba(255, 255, 255, 0.5)' }}>
                    <CircularProgress style={{ color: 'blue' }} />
                    </div> :

                        <DataGrid
                        rows={rows}
                        columns={columns}
                        pagination
                        paginationMode="server"
                        paginationModel={paginationModel}
                        onPaginationModelChange={model => setPaginationModel(model)}
                        rowCount={data?.totalCount || 0}
                        pageSizeOptions={[5, 10, 20, 100]}
                        loading={isLoading}
                        />
                    }

                </div>
            </section>
        </div>
    </div>
</>
);
}

export default AccountsCenter