import React, { useState, useRef, useEffect } from 'react';
import { Html5QrcodeScanner } from 'html5-qrcode';
import axiosConfig from '../../axiosConfig';
import Sidebar from '../components/Sidebar';
import { Modal, Box, Typography, Button } from '@mui/material';
import dayjs from 'dayjs';
import Swal from 'sweetalert2';
import _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCertificate } from '@fortawesome/free-solid-svg-icons';

function CertificateScan() {
    const [isSidebarOpen, setIsSidebarOpen] = useState(true);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [selectedCertificate, setSelectedCertificate] = useState(null);
    const [studentIdInput, setStudentIdInput] = useState('');
    const [certificates, setCertificates] = useState([]);
    const [certificate, setCertificate] = useState({
        valid: false,
        data: {
            Name: '',
            CertificateId: '',
            CertificateURL: '',
            DateGenerated: '',
            CreatedBy: ''
        }
    });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    const [scanning, setScanning] = useState(false);
    const [lastAlertTime, setLastAlertTime] = useState(0);
    const alertTimeout = 60000; 
    const qrRef = useRef(null);

    const toggleSidebar = () => setIsSidebarOpen(!isSidebarOpen);

    useEffect(() => {
        const config = { fps: 5, qrbox: { width: 250, height: 250 } };
        const html5QrCode = new Html5QrcodeScanner(
            "qr-reader",
            config,
            false
        );
        html5QrCode.render(onScanSuccess, _.debounce(onScanFailure, 2000));

        return () => {
            html5QrCode.clear();
        };
    }, []);

    const onScanSuccess = async (decodedText) => {
        setScanning(false);
        try {
            const response = await axiosConfig.get('https://e-cms-capstone-backend-hazel.vercel.app/verify-certificates/scan-certificate', {
                headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` },
                params: { encryptedData: decodedText }
            });
            setCertificate(response.data);
            setError('');
        } catch (err) {
            const errorMessage = err.response ? err.response.data.message || 'Failed to fetch certificate' : 'An error occurred while fetching the certificate';
            Swal.fire({
                title: 'Error!',
                text: errorMessage,
                icon: 'error',
                confirmButtonText: 'OK'
            });
            setCertificate({
                valid: false,
                data: {
                    Name: 'Error, please try again.',
                    CertificateId: 'Error, please try again.',
                    CertificateURL: 'Error, please try again.',
                    DateGenerated: 'Error, please try again.',
                    CreatedBy:'Error, please try again.'
                }
            });
        }
    };

    const fetchCertificates = async () => {
        setLoading(true);
        try {
            const response = await axiosConfig.get(`https://e-cms-capstone-backend-hazel.vercel.app/students/student-profile?studentId=${studentIdInput}`, {
                headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
            });
            setCertificates(response.data.certificates);
            setLoading(false);
        } catch (error) {
            setLoading(false);
            Swal.fire('Error', 'Failed to fetch certificates', 'error');
        }
    };

    const openModal = (cert) => {
        setSelectedCertificate(cert);
        setModalIsOpen(true);
    };

    const closeModal = () => {
        setModalIsOpen(false);
        setSelectedCertificate(null);
    };

    const onScanFailure = (error) => {
        console.error(`QR Code not detectable: ${error}`);
        setScanning(false);
        const now = new Date().getTime();
        if (now - lastAlertTime > alertTimeout) {
            Swal.fire({
                title: 'Scanning Error',
                text: 'Failed to scan QR Code. Please ensure the QR code is stable and well-lit.',
                icon: 'error',
                confirmButtonText: 'OK'
            });
            setLastAlertTime(now);
        }
    };

    const renderInput = (value, isError = false) => (
        <input
            type="text"
            value={value}
            readOnly
            className={`w-full p-2 border border-gray-300 rounded mt-1 ${isError ? 'text-red-500' : ''}`}
        />
    );

    const handleScanClick = () => {
        setScanning(true); // Set scanning to true when scan starts
    };

    return (
        <>
            <Sidebar isCollapsed={!isSidebarOpen} toggleSidebar={toggleSidebar} />
            <div className={`bg-[#edf0f7] min-h-screen transition-padding duration-300 ${isSidebarOpen ? 'pl-64' : 'pl-16'}`}>
                <h1 className="text-4xl font-semibold text-[#3a53a5] bg-[#D9D9D9] text-center p-3">VERIFY CERTIFICATE</h1>
                <div className="flex flex-col md:flex-row justify-center items-start p-4">
                    <div id="qr-reader" ref={qrRef} className="w-full md:w-1/2" onClick={handleScanClick}></div>
                    <div className="w-full md:w-1/2 p-4 bg-white rounded-lg shadow-sm md:ml-4 mt-4 md:mt-0">
                        <h2 className="text-lg font-bold mb-3">Certificate Info</h2>
                        <button className={`px-4 py-2 rounded text-white font-bold ${scanning ? 'bg-gray-500' : certificate.valid ? 'bg-green-500' : 'bg-red-500'}`} disabled>
                            {scanning ? 'SCANNING...' : certificate.valid ? 'VALID' : 'INVALID'}
                        </button>
                        <div className="mt-3">
                            <label className="block font-medium">Name</label>
                            {renderInput(certificate.data.Name, certificate.data.Name === 'Error, please try again.')}
                        </div>
                        <div className="mt-3">
                            <label className="block font-medium">Certificate ID</label>
                            {renderInput(certificate.data.CertificateId, certificate.data.CertificateId === 'Error, please try again.')}
                        </div>
                        <div className="mt-3">
                            <label className="block font-medium">Certificate URL</label>
                            {renderInput(certificate.data.CertificateURL, certificate.data.CertificateURL === 'Error, please try again.')}
                        </div>
                        <div className="mt-3">
                            <label className="block font-medium">Date Generated</label>
                            {renderInput(certificate.data.DateGenerated ? (certificate.data.DateGenerated === 'Error, please try again.' ? certificate.data.DateGenerated : dayjs(certificate.data.DateGenerated).format('MMMM D, YYYY')) : '', certificate.data.DateGenerated === 'Error, please try again.')}
                        </div>
                        <div className="mt-3">
                            <label className="block font-medium">Generated By</label>
                            {renderInput(certificate.data.CreatedBy, certificate.data.CreatedBy === 'Error, please try again.')}
                        </div>
                        <div className="bg-slate-100 p-4 mt-5 rounded-lg shadow-sm">
                        <h2 className="text-lg font-bold mb-3">Search by Student ID</h2>
                        <div className="mt-3">
                            <input
                                type="text"
                                placeholder="Enter Student ID"
                                value={studentIdInput}
                                onChange={(e) => setStudentIdInput(e.target.value)}
                                className="border p-2 w-full"
                            />
                            <button onClick={fetchCertificates} className="bg-blue-500 text-white p-2 mt-2 w-full">Search</button>
                        </div>
                        {loading ? <p>Loading...</p> : 
                            <div>
                                <h3 className="text-lg font-semibold mb-4">CERTIFICATES ({certificates.length})</h3>
                                <div className="flex overflow-x-auto">
                                    {certificates.map((cert) => (
                                        <div key={cert.certificateId} className="flex-shrink-0 mr-4">
                                            <div className="w-32 h-32 bg-gray-300 rounded-lg flex items-center justify-center text-6xl">
                                                <FontAwesomeIcon icon={faCertificate} />
                                            </div>
                                            <div className="text-center mt-2 text-sm w-32">
                                                <button onClick={() => openModal(cert)} className="text-blue-700 hover:underline">
                                                    {cert.certificateId}
                                                </button>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        }
                        </div>
                    </div>
                </div>
            </div>
            <Modal
                open={modalIsOpen}
                onClose={closeModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                    width: 400,
                    bgcolor: 'background.paper',
                    border: '2px solid #000',
                    boxShadow: 24,
                    p: 4,
                }}>
                    {selectedCertificate && (
                        <>
                            <Typography id="modal-modal-title" variant="h6" component="h2">
                                {selectedCertificate.Name}
                            </Typography>
                            <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                                ID: {selectedCertificate.certificateId}
                            </Typography>
                            <Typography sx={{ mt: 2 }}>
                                URL: <a href={selectedCertificate.certificateURL} target="_blank" rel="noopener noreferrer">View Certificate</a>
                            </Typography>
                            <Typography sx={{ mt: 2 }}>
                                Date Generated: {dayjs(selectedCertificate.DateGenerated).format('MMMM D, YYYY')}
                            </Typography>
                            <Typography sx={{ mt: 2 }}>
                                Created By: {selectedCertificate.CreatedBy}
                            </Typography>
                        </>
                    )}
                </Box>
            </Modal>
        </>
    );
    
}

export default CertificateScan;