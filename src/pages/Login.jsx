import React, {useState} from 'react'
import { useMailData } from '../components/contexts/MailDataContext'
import { useNavigate } from 'react-router-dom'
import axiosConfig from '../../axiosConfig'
import { useQuery } from '@tanstack/react-query'
import Swal from 'sweetalert2';

import CircularProgress from '@mui/material/CircularProgress';


function Login() {
    const { updateUserRole, updateUserName } = useMailData();

    const [username,setUsername] = useState('')
    const [password,setPassword] = useState('')
    const navigate = useNavigate();

    const {
        isPending,
        isError
    } = useQuery({
        queryKey: ['users'],
        queryFn: async () => {
            return await axiosConfig.get('https://e-cms-capstone-backend-hazel.vercel.app/users/user-list')
        }
    })

        if (isPending) {
            return (
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh', backgroundColor: 'rgba(255, 255, 255, 0.5)' }}>
                    <CircularProgress style={{ color: 'blue' }} />
                </div>
            );
        }
        if (isError) {
            return <h1>Fetching Error</h1>
        }

        const handleLogin = async (event) => {
            event.preventDefault();
            try {
                const response = await axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/users/login', { username, password });
                const { token, role, username: responseUserName } = response.data;
    
                localStorage.setItem('token', token); // Store token in localStorage
                updateUserRole(role);
                updateUserName(responseUserName);
    
                Swal.fire({
                    icon: 'success',
                    title: 'Logged in successfully!',
                    text: 'Redirecting to Dashboard...',
                    showConfirmButton: false,
                    timer: 1500
                });
    
                setTimeout(() => {
                    navigate('/dashboard'); // Navigate to dashboard after login
                }, 1500);
            } catch (error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Login Failed',
                    text: 'Username or password incorrect.',
                    confirmButtonText: 'OK'
                });
            }
        };


        return (
            <div className='flex flex-col md:flex-row w-full h-screen'>
        
                <div className='w-full md:w-1/2 h-full bg-slate-50 flex justify-center items-center relative'>
                    <img className='absolute top-0 left-0 w-full h-full object-cover' src="assets/Login_Background.jpg" alt="Form Background Image" />
                    <form className='z-10 text-center bg-white border-2 rounded-lg border-blue-950 w-full max-w-lg p-9'
                    onSubmit={handleLogin}>
                        <img className='object-scale-down h-28 w-full pb-2' src="assets/CMO_Seal.png" alt="CMO Seal"/>
                        <h1 className='text-xl text-blue-900 pb-4'>SIGN IN</h1>
                        {/* Username Input */}
                        <div className="text-left w-3/4 mx-auto">
                            <label className='block'>Username</label>
                            <input className='w-full h-10 text-black bg-sky-100 p-2'
                            type='text'
                            placeholder='Enter Username'
                            value={username}
                            onChange={(e) => setUsername(e.target.value)} />
                        </div>
                        <br />
                        {/* Password Input */}
                        <div className="text-left w-3/4 mx-auto">
                            <label className='block'>Password</label>
                            <input className='w-full h-10 text-black bg-sky-100 p-2'
                            type='password'
                            placeholder='Enter Password'
                            value={password}
                            onChange={(e) => setPassword(e.target.value)} />
                        </div>
                        <br />
                        {/* Button */}
                        <button className='w-1/2 h-12 text-white border rounded-lg bg-blue-900 hover:bg-blue-950'
                        type='submit'>LOG IN</button>
                    </form>
                </div>
        
                <div className='w-full hidden md:flex md:w-1/2 h-fulljustify-center items-center bg-blue-950'>
                    <img className='object-scale-down h-48 p-2 w-full' src="assets/XU_Logotype.png" alt="XU Logotype"/>
                </div>
        
            </div>
        )
}

export default Login
