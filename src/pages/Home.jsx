import React from 'react'
import Navbar from '../components/Navbar';

function Home() {
  return (
    <> <Navbar/>
    <div className='w-full h-screen bg-[#1a1a1a] text-white flex justify-center items-center'>
      <h2 className='text-3xl'>HOME PAGE</h2>
     </div>
     </>
  )
}

export default Home

