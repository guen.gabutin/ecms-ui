import React, { useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserCircle, faArrowLeft, faCertificate } from '@fortawesome/free-solid-svg-icons';
import CircularProgress from '@mui/material/CircularProgress';

import Sidebar from '../components/Sidebar';

import axiosConfig from '../../axiosConfig'
import { useQuery } from '@tanstack/react-query';

const StudentProfile = () => {
  const { studentId } = useParams();
  const [isSidebarOpen, setIsSidebarOpen] = useState(true);

  const navigate = useNavigate();

  const toggleSidebar = () => setIsSidebarOpen(!isSidebarOpen);

  const fetchStudentData = async ({ queryKey }) => {
    const [_key, { studentId }] = queryKey;
    const token = localStorage.getItem('token');
    if (!token) {
      throw new Error('Authentication token is not available.');
    }
    const response = await axiosConfig.get(`https://e-cms-capstone-backend-hazel.vercel.app/students/student-profile?studentId=${studentId}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return response.data;
  };

  const { data: studentData, isError, error, isLoading } = useQuery({
    queryKey: ['studentData', { studentId }],
    queryFn: fetchStudentData,
  });

  if (isError) return <div>Error: {error.message}</div>;

  // Determine button style and text based on certificatesTotal
  let buttonStyle, buttonText;
  if (studentData) {
    if (studentData.certificatesTotal > 4) {
      buttonStyle = "bg-blue-500"; // Blue for SPECIAL
      buttonText = "SPECIAL";
    } else if (studentData.certificatesTotal === 4) {
      buttonStyle = "bg-green-500"; // Green (or any other color) for COMPLETE
      buttonText = "COMPLETE";
    } else {
      buttonStyle = "bg-red-500"; // Red for INCOMPLETE
      buttonText = "INCOMPLETE";
    }
  }

  return (
    <>
      <Sidebar isCollapsed={!isSidebarOpen} toggleSidebar={toggleSidebar} />

      <div className={`bg-[#edf0f7] min-h-screen transition-padding duration-300 ${isSidebarOpen ? 'pl-64' : 'pl-16'}`}>
        <h1 className="text-4xl font-semibold text-[#3a53a5] bg-[#D9D9D9] text-center p-3">STUDENT RECORDS</h1>

        <div className="bg-gray-100 min-h-screen p-6">
          <div className="bg-white p-6 rounded-lg shadow-lg mx-auto">
            <div className="flex justify-between items-center mb-6">
              <button onClick={() => navigate(-1)} className="flex items-center text-blue-600">
                <FontAwesomeIcon icon={faArrowLeft} className="mr-2" />
                <span>COLLEGE RECORDS</span>
              </button>
            </div>
            
            {isLoading ? (
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '50vh', backgroundColor: 'rgba(255, 255, 255, 0.5)' }}>
                  <CircularProgress style={{ color: 'blue' }} />
                </div>
            ) : (
              <>
                {/* Student Info */}
                <div className="bg-blue-100 p-4 rounded-lg flex items-center mb-6">
                  <FontAwesomeIcon icon={faUserCircle} size="6x" className="text-gray-400" />
                  <div className="ml-4">
                    <h2 className="text-2xl font-bold">{studentData.studentName}</h2>
                    <p className="text-sm">{`${studentData.studentId} - ${studentData.departmentYearStanding}`}</p>
                    <button className={`mt-2 px-4 py-2 text-white rounded ${buttonStyle}`} disabled>
                      {buttonText}
                    </button>
                  </div>
                </div>

                {/* Certificates Section */}
                <div>
                  <h3 className="text-lg font-semibold mb-4">CERTIFICATES ({studentData.certificatesTotal})</h3>
                  <div className="flex overflow-x-auto">
                    {studentData.certificates.map((cert) => (
                        <div key={cert.certificateId} className="flex-shrink-0 mr-4">
                        <div className="w-32 h-32 bg-gray-300 rounded-lg flex items-center justify-center text-6xl">
                          {/* Using the faFile icon from Font Awesome */}
                          <FontAwesomeIcon icon={faCertificate} />
                        </div>
                        <div className="text-center mt-2 text-sm w-32">
                          {/* Ensure this text respects the width of the above icon container */}
                          <a href={cert.certificateURL} target="_blank" rel="noopener noreferrer">
                            {cert.certificateId}
                          </a>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </>
            )}

          </div>
        </div>
      </div>
    </>
  );
};

export default StudentProfile;