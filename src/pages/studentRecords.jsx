import React, {useState} from 'react';
import { useRoutes } from 'react-router-dom';
import Sidebar from '../components/Sidebar';
import DepartmentDetail from './DepartmentDetail';
import Colleges from '../components/Colleges';

import { useMailData } from '../components/contexts/MailDataContext';

const StudentRecords = () => {
  
  const { departmentDetails } = useMailData();
  const [isSidebarOpen, setIsSidebarOpen] = useState(true);

  const toggleSidebar = () => {
      setIsSidebarOpen(!isSidebarOpen);
  };

    let element = useRoutes([
        { path: '/', element: <Colleges />, index: true },
        { path: ':collegeName', element: <DepartmentDetail departmentData={departmentDetails} /> },
      ]);

      
      return (
        <>
            <Sidebar isCollapsed={!isSidebarOpen} toggleSidebar={toggleSidebar} />
            <div className={`bg-[#edf0f7] min-h-screen transition-padding duration-300 ${isSidebarOpen ? 'pl-64' : 'pl-16'}`}>
                <h1 className="text-4xl font-semibold text-[#3a53a5] bg-[#D9D9D9] mb- text-center p-3">STUDENT RECORDS</h1>
                <div className="bg-gray-100 min-h-screen p-6">
                    <div className="bg-white p-6 rounded-lg shadow-lg mx-auto">
                        <>{element}</>
                    </div>
                </div>
            </div>
        </>
    );
};

export default StudentRecords;