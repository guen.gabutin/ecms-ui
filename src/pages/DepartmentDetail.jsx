import React, { useState, useEffect, useMemo } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import axiosConfig from '../../axiosConfig'
import { useMailData } from '../components/contexts/MailDataContext';

import { DataGrid } from '@mui/x-data-grid';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { Button } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faSearch, faArrowLeft, faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import Swal from 'sweetalert2';


  const DepartmentDetail = ({ departmentData }) => {
    const { userRole, majorDetails } = useMailData();
    const [selectedCollege, setSelectedCollege] = useState('');
    const [selectedDepartment, setSelectedDepartment] = useState('');
    const navigate = useNavigate();
    const { collegeName } = useParams();

    const [isModalOpen, setIsModalOpen] = useState(false);
    const [modalMode, setModalMode] = useState('add'); // 'add' or 'edit'
    const [currentStudent, setCurrentStudent] = useState(null); // Event being edited
    const [formData, setFormData] = useState({
      studentId: '',
      college: '',
      department: '',
      // program: '',
      major: '',
      email: '',
      firstName: '',
      lastName: '',
      yearStanding: ''
  });

  const [searchKeyword, setSearchKeyword] = useState('');
  const [debouncedSearchKeyword, setDebouncedSearchKeyword] = useState(searchKeyword);

  // Debounce searchKeyword
  useEffect(() => {
      const handler = setTimeout(() => {
          setDebouncedSearchKeyword(searchKeyword);
      }, 500); // 500ms delay for debouncing

      return () => {
          clearTimeout(handler);
      };
  }, [searchKeyword]);

    // Add console.log to check the value of collegeName
    // console.log("Provided collegeName:", collegeName);
  
    const specificDepartmentData = Object.values(departmentData).find(
      department => department.name.replace(/ /g, '') === collegeName
    ) || { name: 'Unknown', departments: [] };

    // Add console.log to check the keys in departmentDetails
    // console.log("Keys in departmentDetails:", Object.keys(departmentData));

    // console.log("specificDepartmentData:", specificDepartmentData);
  
  // Handler for changing the selected department from the dropdown.
  const handleDepartmentChange = (event) => {
    setSelectedDepartment(event.target.value);
  };

  const handleChange = (event) => {
    const { name, value } = event.target;

    // If the college is changed, reset the department selection
    if (name === 'college') {
      setSelectedCollege(value);
      setFormData({ ...formData, college: value, department: '' });
    } else {
      setFormData({ ...formData, [name]: value });
    }
  };


        // Modal style
        const style = {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            bgcolor: 'background.paper',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4,
        };

    // Initialize pagination model state
    const [paginationModel, setPaginationModel] = useState({
      page: 0, 
      pageSize: 10, // Number of items per page
    });

        // Add console.log to check the paginationModel
        // console.log("paginationModel:", paginationModel);

    const { data, error, isLoading } = useQuery({
      queryKey: ['students', paginationModel, specificDepartmentData.name, selectedDepartment, debouncedSearchKeyword],
      queryFn: () => 
        axiosConfig
        .get(`https://e-cms-capstone-backend-hazel.vercel.app/students/student-list?page=${paginationModel.page + 1}&limit=${paginationModel.pageSize}&college=${encodeURIComponent(specificDepartmentData.name)}&department=${encodeURIComponent(selectedDepartment)}&keyword=${encodeURIComponent(debouncedSearchKeyword)}`)
        .then(res => res.data),
        keepPreviousData: true,
    });

    if (error) return <div>An error occurred: {error.message}</div>;

    // Add console.log to check the fetched data
    // console.log("Fetched data:", data);


    const columns = useMemo(() => {
      const baseColumns = [
        {
          field: 'studentId',
          headerName: 'Student ID',
          width: 130,
          renderCell: (params) => (
            <a
              onClick={() => navigate(`/students/student-profile/${params.value}`)}
              style={{ cursor: 'pointer', color: 'blue', textDecoration: 'underline' }}
            >
              {params.value}
            </a>
          ),
        },
        { field: 'lastName', headerName: 'Last Name', width: 130 },
        { field: 'firstName', headerName: 'First Name', width: 130 },
        { field: 'department', headerName: 'Department', width: 200 },
        { field: 'yearStanding', headerName: 'Year Standing', width: 130 },
      ];
  
      if (userRole !== 'StudentAssistant') {
        baseColumns.push({
          field: 'actions',
          headerName: 'Actions',
          sortable: false,
          width: 150,
          renderCell: (params) => {
            return (
              <>
                <button onClick={() => handleEditStudent(params.row)} style={{ marginRight: '10px' }}>
                  <FontAwesomeIcon icon={faEdit} />
                </button>
                <button onClick={() => handleDeleteStudent(params.id)}>
                  <FontAwesomeIcon icon={faTrashAlt} />
                </button>
              </>
            );
          },
        });
      }
      return baseColumns;
    }, [userRole]); // Depend on userRole to re-calculate columns when it changes

  const rows = data?.data
    ? data.data.map(student => ({
        id: student._id,
        studentId: student.studentId,
        college: student.college,
        department: student.department,
        // program: student.program,
        major: student.major,
        email: student.email,
        firstName: student.firstName,
        lastName: student.lastName,
        yearStanding: student.yearStanding
      }))
    : [];

    // console.log(rows);

  const handleAddStudent = () => {
    setModalMode('add');
    setCurrentStudent(null);
    setFormData({
      studentId: '',
      college: '',
      department: '',
      // program: '',
      major: '',
      email: '',
      firstName: '',
      lastName: '',
      yearStanding: ''
    });
    setIsModalOpen(true);
};

const handleEditStudent = (student) => {
  setModalMode('edit');
  setCurrentStudent(student);
  setSelectedCollege(student.college);
  setFormData({
    studentId: student.studentId,
    college: student.college,
    department: student.department,
    // program: student.program,
    major: student.major,
    email: student.email,
    firstName: student.firstName,
    lastName: student.lastName,
    yearStanding: student.yearStanding
  });
  setIsModalOpen(true);
};

const handleDeleteStudent = async (studentId) => {
  const token = localStorage.getItem('token');

  // Show a confirmation dialog before deletion
  const result = await Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
  });

  // Proceed with deletion only if the user confirmed
  if (result.isConfirmed) {
      try {
          const response = await axiosConfig.delete(`https://e-cms-capstone-backend-hazel.vercel.app/students/delete-student/${studentId}`, {
              headers: {
                  'Authorization': `Bearer ${token}`
              }
          });
          
          if (response.status === 200) {
              Swal.fire(
                  'Deleted!',
                  'Student has been deleted.',
                  'success'
              );
              queryClient.invalidateQueries(['students']); // Refetch the event list
          }
      } catch (error) {
          console.error('Error deleting student:', error);
          Swal.fire(
              'Error!',
              error.response?.data?.message || 'An error occurred while deleting student.',
              'error'
          );
      }
  }
};

const handleClose = () => {
  // console.log('Closing modal and resetting form data');
  setIsModalOpen(false);
  // Reset form data when modal closes
  setFormData({
    studentId: '',
    college: '',
    department: '',
    // program: '',
    major: '',
    email: '',
    firstName: '',
    lastName: '',
    yearStanding: ''
  });
};

  const handleSearch = () => {
    const keyword = event.target.value;
    setSearchKeyword(keyword);
    setPaginationModel({ page: 0, pageSize: paginationModel.pageSize });
  };

  const queryClient = useQueryClient();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const token = localStorage.getItem('token');

  if (modalMode === 'add') {
                
    // Simple front-end validation
    if (!formData.studentId || !formData.department || !formData.major || !formData.email || !formData.firstName || !formData.lastName || !formData.yearStanding) {
        Swal.fire('Error', 'Please fill in all required fields.', 'error');
        return;
    }

    // Validate studentId to ensure it contains only numbers
    const studentIdRegex = /^\d+$/; // Regex to check if the string contains only digits
    if (!studentIdRegex.test(formData.studentId)) {
        Swal.fire('Error', 'Student ID must contain only numbers.', 'error');
        return;
    }

    // Check if email format is correct
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      if (!emailRegex.test(formData.email)) {
          Swal.fire('Error', 'Please enter a valid email address.', 'error');
          return;
    }

    // Construct the user data object
    const studentData = {
      studentId: formData.studentId,
      college: specificDepartmentData.name,
      department: formData.department,
      // program: formData.program,
      major: formData.major,
      email: formData.email,
      firstName: formData.firstName,
      lastName: formData.lastName,
      yearStanding: formData.yearStanding
    };

    // Attempt to create a new user
    try {
        const response = await axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/students/add-student', studentData, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        // Check the response status code
        if (response.status === 201) {
            Swal.fire('Success!', 'Student created successfully.', 'success');
            setIsModalOpen(false); // Close the modal
            queryClient.invalidateQueries(['students']); // Refetch the user list
        }
      } catch (error) {
        if (error.response && error.response.status === 400 && error.response.data.error === 'Student ID already exists') {
            Swal.fire('Error!', 'Student ID already exists.', 'error');
        } else {
            console.error('Error adding student:', error);
            Swal.fire('Error!', error.response?.data?.message || 'An error occurred while adding the student.', 'error');
        }
      }
    } else if (modalMode === 'edit' && currentStudent) {

    setIsModalOpen(false);
    const result = await Swal.fire({
        title: 'Are you sure?',
        text: "You are about to update the user's information.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, update it!',
    });

    if (result.isConfirmed) {
        try {
            const response = await axiosConfig.put(`https://e-cms-capstone-backend-hazel.vercel.app/students/edit-student/${currentStudent.id}`, {
                studentId: formData.studentId,
                college: formData.college,
                department: formData.department,
                // program: formData.program,
                major: formData.major,
                email: formData.email,
                firstName: formData.firstName,
                lastName: formData.lastName,
                yearStanding: formData.yearStanding
            }, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            if (response.status === 200) {
                Swal.fire('Updated!', 'Student has been updated.', 'success');
                queryClient.invalidateQueries(['users']);
            }
        } catch (error) {
            console.error('Error updating student:', error);
            Swal.fire('Error!', 'An error occurred while updating student.', 'error');
        }
    }
}
};

  // Logging rowCount
  // console.log("rowCount:", data?.totalCount || 0);

  return (
    <>

    <Modal open={isModalOpen} onClose={handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description" sx={{ zIndex: 600}}>
        <Box sx={style}>
            <h2 id="modal-modal-title" className="text-lg font-bold mb-4">{modalMode === 'add' ? 'Add New Student' : 'Edit Student'}</h2>
            <form onSubmit={handleSubmit}>
                <input
                    name="studentId"
                    type="text"
                    placeholder="Student ID"
                    value={formData.studentId}
                    onChange={handleChange}
                    className="block w-full mb-3 px-3 py-1.5 border rounded"
                    disabled={modalMode === 'edit'}
                />
                <select
                  name="college"
                  value={formData.college}
                  onChange={handleChange}
                  className="block w-full mb-3 px-3 py-2 border rounded"
                >
                  <option value="" disabled>Select College</option>
                  {Object.keys(departmentData).map((key) => (
                    <option key={key} value={departmentData[key].name}>{departmentData[key].name}</option>
                  ))}
              </select>
                <select
                    name="department"
                    value={formData.department}
                    onChange={handleChange}
                    className="block w-full mb-3 px-3 py-2 border rounded"
                >
                    <option value="" disabled>Select Department</option>
                    {formData.college && departmentData[selectedCollege.replace(/ /g, '')]?.departments.map((department) => (
                      <option key={department} value={department}>{department}</option>
                    ))}
                </select>
                {/* <select
                    name="program"
                    value={formData.program}
                    onChange={handleChange}
                    className="block w-full mb-3 px-3 py-2 border rounded"
                >
                    <option value="" disabled>Select Program type</option>
                    <option value="Undergraduate">Undergraduate</option>
                    <option value="Graduate">Graduate</option>
                </select> */}
                <select
                  name="major"
                  value={formData.major}
                  onChange={handleChange}
                  className="block w-full mb-3 px-3 py-2 border rounded"
                  required
                >
                  <option value="">Select Major</option>
                  {majorDetails.MajorsList.groups.map((group) => (
                    <optgroup key={group.label} label={group.label}>
                      {group.majors.map((major) => (
                        <option key={major} value={major}>{major}</option>
                      ))}
                    </optgroup>
                  ))}
                </select>
                <input
                    name="email"
                    type="email"
                    placeholder="Email"
                    value={formData.email}
                    onChange={handleChange}
                    className="block w-full mb-3 px-3 py-1.5 border rounded"
                />
                <input
                    name="firstName"
                    type="text"
                    placeholder="First Name"
                    value={formData.firstName}
                    onChange={handleChange}
                    className="block w-full mb-3 px-3 py-1.5 border rounded"
                />
                <input
                    name="lastName"
                    type="text"
                    placeholder="Last Name"
                    value={formData.lastName}
                    onChange={handleChange}
                    className="block w-full mb-3 px-3 py-1.5 border rounded"
                />
                <select
                    name="yearStanding"
                    value={formData.yearStanding}
                    onChange={handleChange}
                    className="block w-full mb-3 px-3 py-2 border rounded"
                >
                    <option value="" disabled>Select Year Level</option>
                    <option value="1">1st</option>
                    <option value="2">2nd</option>
                    <option value="3">3rd</option>
                    <option value="4">4th</option>
                </select>
                <button type="button" onClick={handleClose} className="px-4 py-2 bg-red-500 text-white rounded hover:bg-red-600">Cancel</button>
                <button type="submit" className="ml-2 px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600">
                    {modalMode === 'add' ? 'Add Student' : 'Update Student'}
                </button>
            </form>
        </Box>
    </Modal>

     <div style={{ marginBottom: '20px' }}>
        <Button
          className="text-blue-500 hover:text-blue-700 p-2 rounded-lg"
          startIcon={<FontAwesomeIcon icon={faArrowLeft} />}
          onClick={() => navigate(-1)}
        >
          Back
        </Button>
        <h2 className="text-2xl flex-grow text-center mb-10">
          {`College of ${specificDepartmentData.name}`}
        </h2>

    </div>
      
    <div style={{ marginBottom: '20px' }}>
      <div className='flex justify-between items-center'>
        <div>
          {userRole !== 'StudentAssistant' && (
          <Button
            startIcon={<FontAwesomeIcon icon={faPlus} />}
            color="primary"
            variant="contained"
            onClick={handleAddStudent}
          >
            Add Student
          </Button>
        )}
        </div>
        
        <div className="flex items-center">
          <div className="flex items-center mr-4">
            <FontAwesomeIcon icon={faSearch} className="text-gray-400 mr-2" />
            <TextField 
              id="search-input"
              className="mr-4"
              label="Search"
              variant="outlined"
              size="small"
              value={searchKeyword}
              onChange={handleSearch}
            />
          </div>

          <select
            className="border-2 rounded-lg p-2"
            value={selectedDepartment}
            onChange={handleDepartmentChange}
          >
            <option value="">Select Department</option>
            {specificDepartmentData.departments.map((department, index) => (
              <option key={index} value={department}>{department}</option>
            ))}
          </select>
        </div>
      </div>
    </div>

      {isLoading ? (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '25vh', backgroundColor: 'rgba(255, 255, 255, 0.5)' }}>
        <CircularProgress style={{ color: 'blue' }} />
        </div>
      ) : error ? (
        <p>An error has occurred: {error.message}</p>
      ) : (
        <div style={{ height: 400, width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            pagination
            paginationMode="server"
            paginationModel={paginationModel}
            onPaginationModelChange={(model) => setPaginationModel(model)}
            rowCount={data?.totalCount || 0}
            pageSizeOptions={[10, 20, 100]}
            loading={isLoading}
          />
        </div>
      )}
    </>
  );
};

export default DepartmentDetail;