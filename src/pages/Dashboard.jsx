import React, { useState, useEffect, useMemo }from 'react';
import Sidebar from '../components/Sidebar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faUserGraduate,
        faPlus,
        faSearch,
        faCertificate,
        faCalendarWeek,
        faCalendarAlt,
        faEdit,
        faTrashAlt
      } from '@fortawesome/free-solid-svg-icons';

import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';

import Swal from 'sweetalert2';

import { DateRangePicker } from 'rsuite';

import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';

import { useQuery, useQueryClient } from '@tanstack/react-query';
import axiosConfig from '../../axiosConfig'
import { useMailData } from '../components/contexts/MailDataContext';


const Dashboard = () => {
  const { userRole } = useMailData();

  const queryClient = useQueryClient();

  const [dateRange, setDateRange] = useState([null, null]);

  const [isSidebarOpen, setIsSidebarOpen] = useState(true);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalMode, setModalMode] = useState('add'); // 'add' or 'edit'
  const [currentEvent, setCurrentEvent] = useState(null); // Event being edited
  const [formData, setFormData] = useState({
    eventDate: '',
    department: '',
    description: '',
    batch: '',
    yearLevel: '',
    venue: '',
    inCharge: ''
});

  const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

    // Define the fetch function
    const fetchCardTotals = async () => {
      const token = localStorage.getItem('token');

      const { data } = await axiosConfig.get('https://e-cms-capstone-backend-hazel.vercel.app/dashboard-cards/card-totals', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
    
      return data;
    };

    const handleAddEvent = () => {
      setModalMode('add');
      setCurrentEvent(null);
      setFormData({
        eventDate: '',
        department: '',
        description: '',
        batch: '',
        yearLevel: '',
        venue: '',
        inCharge: ''
      });
      setIsModalOpen(true);
  };

  const handleEditEvent = (event) => {
    setModalMode('edit');
    setCurrentEvent(event);
  
    // Assuming event.eventDate is in 'dd/mm/yyyy' format
    let [day, month, year] = event.eventDate.split('/');
    let formattedDate = `${year}-${month}-${day}`; // Convert to 'yyyy-MM-dd' format
  
    setFormData({
      eventDate: formattedDate,
      department: event.department,
      description: event.description,
      batch: event.batch,
      yearLevel: event.yearLevel,
      venue: event.venue,
      inCharge: event.inCharge
    });
  
    setIsModalOpen(true);
  };

  // Log formData whenever it changes
  // useEffect(() => {
  //   console.log(formData);
  // }, [formData]);

  const handleDeleteEvent = async (eventId) => {
    const token = localStorage.getItem('token');

    // Show a confirmation dialog before deletion
    const result = await Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    });

    // Proceed with deletion only if the user confirmed
    if (result.isConfirmed) {
        try {
            const response = await axiosConfig.delete(`https://e-cms-capstone-backend-hazel.vercel.app/cmo-events/delete-event/${eventId}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            
            if (response.status === 200) {
                Swal.fire(
                    'Deleted!',
                    'The event has been deleted.',
                    'success'
                );
                queryClient.invalidateQueries(['cmoevents']); // Refetch the event list
            }
        } catch (error) {
            console.error('Error deleting event:', error);
            Swal.fire(
                'Error!',
                error.response?.data?.message || 'An error occurred while deleting the event.',
                'error'
            );
        }
    }
};

  const handleSubmit = async (event) => {
    event.preventDefault();
    const token = localStorage.getItem('token');

    if (modalMode === 'add') {
        
        // Simple front-end validation
        if (!formData.eventDate || !formData.department || !formData.description || !formData.batch || !formData.yearLevel || !formData.venue || !formData.inCharge) {
            Swal.fire('Error', 'Please fill in all fields.', 'error');
            return;
        }

        // Construct the event data object
        const eventData = {
            eventDate: formData.eventDate,
            department: formData.department,
            description: formData.description,
            batch: formData.batch,
            yearLevel: formData.yearLevel,
            venue: formData.venue,
            inCharge: formData.inCharge
        };

        // Attempt to create a new event
        try {
            const response = await axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/cmo-events/event', eventData, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            // Check the response status code
            if (response.status === 201) {
                Swal.fire('Success!', 'Event created successfully.', 'success');
                setIsModalOpen(false); // Close the modal
                queryClient.invalidateQueries(['cmoevents']); // Refetch the event list
            }
        } catch (error) {
            console.error('Error creating event:', error);
            Swal.fire('Error!', error.response?.data?.message || 'An error occurred while creating event.', 'error');
        }
    } else if (modalMode === 'edit' && currentEvent) {
        // Since the edit functionality is not in focus for the integration, keep it as is
        setIsModalOpen(false);
        const result = await Swal.fire({
            title: 'Are you sure?',
            text: "You are about to update the event information.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, update it!',
        });

        if (result.isConfirmed) {
            try {
                const response = await axiosConfig.put(`https://e-cms-capstone-backend-hazel.vercel.app/cmo-events/edit-event/${currentEvent.id}`, {
                  eventDate: formData.eventDate,
                  department: formData.department,
                  description: formData.description,
                  batch: formData.batch,
                  yearLevel: formData.yearLevel,
                  venue: formData.venue,
                  inCharge: formData.inCharge
                }, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                });

                if (response.status === 200) {
                    Swal.fire('Updated!', 'The event has been updated.', 'success');
                    queryClient.invalidateQueries(['cmoevents']);
                }
            } catch (error) {
                console.error('Error updating event:', error);
                Swal.fire('Error!', 'An error occurred while updating the event.', 'error');
            }
        }
    }
};

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData(prevFormData => ({ ...prevFormData, [name]: value }));
  };

  const handleClose = () => {
    // console.log('Closing modal and resetting form data');
    setIsModalOpen(false);
    // Reset form data when modal closes
    setFormData({
      eventDate: '',
      department: '',
      description: '',
      batch: '',
      yearLevel: '',
      venue: '',
      inCharge: ''
    });
};

        // Modal style
        const style = {
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          width: 400,
          bgcolor: 'background.paper',
          border: '2px solid #000',
          boxShadow: 24,
          p: 4,
      };

      const [searchKeyword, setSearchKeyword] = useState('');
      const [debouncedSearchKeyword, setDebouncedSearchKeyword] = useState(searchKeyword);
  
          // Debounce searchKeyword
          useEffect(() => {
              const handler = setTimeout(() => {
                  setDebouncedSearchKeyword(searchKeyword);
              }, 500); // 500ms delay for debouncing
      
              return () => {
                  clearTimeout(handler);
              };
          }, [searchKeyword]);

  const handleSearch = () => {
    const keyword = event.target.value;
    setSearchKeyword(keyword);
    setPaginationModel({ page: 0, pageSize: paginationModel.pageSize });
  };

  const { startDate, endDate } = useMemo(() => ({
    startDate: dateRange && dateRange[0] ? dateRange[0].toISOString().split('T')[0] : '',
    endDate: dateRange && dateRange[1] ? dateRange[1].toISOString().split('T')[0] : ''
  }), [dateRange]);

    // Define columns dynamically based on user role
    const columns = useMemo(() => {
      const baseColumns = [
        { field: 'eventDate', headerName: 'Date', width: 130 },
        { field: 'department', headerName: 'Department', width: 200 },
        { field: 'description', headerName: 'Description', width: 250 },
        { field: 'batch', headerName: 'Batch', width: 130 },
        { field: 'yearLevel', headerName: 'Year Level', width: 200 },
        { field: 'venue', headerName: 'Venue', width: 200 },
        { field: 'inCharge', headerName: 'Person in Charge', width: 200 },
      ];
  
      // Add Actions column if user is not a StudentAssistant
      if (userRole !== 'StudentAssistant') {
        baseColumns.push({
          field: 'actions',
          headerName: 'Actions',
          sortable: false,
          width: 150,
          renderCell: (params) => {
            return (
              <>
                <button onClick={() => handleEditEvent(params.row)} style={{ marginRight: '10px' }}>
                  <FontAwesomeIcon icon={faEdit} />
                </button>
                <button onClick={() => handleDeleteEvent(params.id)}>
                  <FontAwesomeIcon icon={faTrashAlt} />
                </button>
              </>
            );
          },
        });
      }
  
      return baseColumns;
    }, [userRole]); // Depend on userRole to re-calculate columns when it changes

        const [paginationModel, setPaginationModel] = useState({
          page: 0,
          pageSize: 10,
        });
        
        // Add console.log to check the paginationModel
        // console.log("paginationModel:", paginationModel);

        const { data, error, isLoading } = useQuery({
          queryKey: ['cmoevents', paginationModel, debouncedSearchKeyword, startDate, endDate],
          // queryKey: ['cmoevents'],
          queryFn: async () => {
            // console.log('Fetching data with paginationModel:', paginationModel);
            const res = await axiosConfig
              .get(`https://e-cms-capstone-backend-hazel.vercel.app/cmo-events/events-list?keyword=${encodeURIComponent(debouncedSearchKeyword)}&startDate=${startDate}&endDate=${endDate}&page=${paginationModel.page + 1}&limit=${paginationModel.pageSize}`);
            return res.data;
          },
          keepPreviousData: true,
        });

        {error && <p>Error: {error.message}</p>}
        
        // Add console.log to check the fetched data
        // console.log("Fetched data:", data);

            // Use useQuery to fetch and cache the data
        const { data: dashboardData, isError } = useQuery({
          queryKey: ['cardTotals'],
          queryFn: fetchCardTotals,
        });
      
        if (isError) return <div>An error occurred</div>;
  
        const rows = data?.data
        ? data.data.map(event => ({
              id: event._id,  // Assuming _id is the MongoDB document ID
              eventDate: event.eventDate 
              ? new Date(event.eventDate).toLocaleDateString('en-GB') // Formats to dd/mm/yyyy
              : '',
              department: event.department,
              description: event.description,
              batch: event.batch,
              yearLevel: event.yearLevel,
              venue: event.venue,
              inCharge: event.inCharge,
          }))
        : [];


        // Logging rowCount
        // console.log("rowCount:", data?.totalCount || 0);


  return (

    <>
      <Sidebar isCollapsed={!isSidebarOpen} toggleSidebar={toggleSidebar} />

      {/* Adjust the padding based on whether the sidebar is open */}
      <div className={`bg-[#edf0f7] min-h-screen transition-padding duration-300 ${isSidebarOpen ? 'pl-64' : 'pl-16'}`}>
        <h1 className="text-4xl font-semibold text-[#3a53a5] bg-[#D9D9D9] mb-6 text-center p-3">DASHBOARD</h1>
          
          {/* Active Students Card */}
          <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-4 mb-6 m-9">
            <div className="bg-white rounded-lg shadow-lg p-6 flex items-center justify-between border-l-4 border-blue-500">
              <div>
                <div className="text-gray-500">ACTIVE STUDENTS</div>
                <div className="text-3xl font-bold">{dashboardData?.totalStudents}</div>
              </div>
              <FontAwesomeIcon icon={faUserGraduate} className="text-[#3a53a5] text-3xl" />
            </div>
              
            <div className="bg-white rounded-lg shadow-lg p-6 flex items-center justify-between border-l-4 border-blue-500">
              <div>
                <div className="text-gray-500">ACTIVE CERTIFICATES</div>
                <div className="text-3xl font-bold">{dashboardData?.totalCertificates}</div>
              </div>
              <FontAwesomeIcon icon={faCertificate} className="text-[#3a53a5] text-3xl" />
            </div>

            <div className="bg-white rounded-lg shadow-lg p-6 flex items-center justify-between border-l-4 border-blue-500">
              <div>
                <div className="text-gray-500">EVENTS - NEXT WEEK</div>
                <div className="text-3xl font-bold">{dashboardData?.eventsNextWeek}</div>
              </div>
              <FontAwesomeIcon icon={faCalendarWeek} className="text-[#3a53a5] text-3xl" />
            </div>

            <div className="bg-white p-6 rounded-lg shadow flex items-center justify-between border-l-4 border-blue-500">
              <div className="flex-grow">
                <span className="text-gray-500 block">EVENTS - THIS MONTH</span>
                <span className="text-3xl font-bold ">{dashboardData?.eventsThisMonth}</span>
              </div>
              <FontAwesomeIcon icon={faCalendarAlt} className="text-[#3a53a5] text-3xl" />
            </div>
          </div>
          
          
          {/* Recollection Dates Section */}

                    {/* Add Event Button */}
                    {userRole !== 'StudentAssistant' && (
                      <div>
                        <button onClick={handleAddEvent} className="flex items-center ml-10 px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600">
                          <FontAwesomeIcon icon={faPlus} className="mr-2" />
                          Add Event
                        </button>
                      </div>
                    )}

                    <Modal open={isModalOpen} onClose={handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description" sx={{ zIndex: 600}}>
                    <Box sx={style}>
                      <h2 id="modal-modal-title" className="text-lg font-bold mb-4">
                          {modalMode === 'add' ? 'Add New Event' : 'Edit Event'}
                      </h2>
                      <form onSubmit={handleSubmit}>
                          <input
                              name="eventDate"
                              type="date"
                              placeholder="Event Date"
                              value={formData.eventDate}
                              onChange={handleChange}
                              className="block w-full mb-3 px-3 py-1.5 border rounded"
                          />
                          <select
                              name="department"
                              value={formData.department}
                              onChange={handleChange}
                              className="block w-full mb-3 px-3 py-2 border rounded"
                          >
                              <option value="" disabled>Select Department</option>
                              <optgroup label="Colleges">
                              <option value="Agriculture">Agriculture</option>
                              <option value="Arts and Sciences">Arts and Sciences</option>
                              <option value="Computer Studies">Computer Studies</option>
                              <option value="Engineering">Engineering</option>
                              <option value="Nursing">Nursing</option>
                              <option value="Business and Management">Business and Management</option>
                              <option value="Education">Education</option>
                              </optgroup>
                              <optgroup label="Others">
                                <option value="NSTP">NSTP</option>
                              </optgroup>
                          </select>
                          <input
                              name="description"
                              type="text"
                              placeholder="Description"
                              value={formData.description}
                              onChange={handleChange}
                              className="block w-full mb-3 px-3 py-1.5 border rounded"
                          />
                          <select
                              name="batch"
                              value={formData.batch}
                              onChange={handleChange}
                              className="block w-full mb-3 px-3 py-2 border rounded"
                          >
                              <option value="" disabled>Select Batch</option>
                              <option value="1">1st</option>
                              <option value="2">2nd</option>
                          </select>
                          <select
                              name="yearLevel"
                              value={formData.yearLevel}
                              onChange={handleChange}
                              className="block w-full mb-3 px-3 py-2 border rounded"
                          >
                              <option value="" disabled>Select Year Level</option>
                              <option value="1">1st</option>
                              <option value="2">2nd</option>
                              <option value="3">3rd</option>
                              <option value="4">4th</option>
                          </select>
                          <input
                              name="venue"
                              type="text"
                              placeholder="Venue"
                              value={formData.venue}
                              onChange={handleChange}
                              className="block w-full mb-3 px-3 py-1.5 border rounded"
                          />
                          <input
                              name="inCharge"
                              type="text"
                              placeholder="Person in Charge"
                              value={formData.inCharge}
                              onChange={handleChange}
                              className="block w-full mb-3 px-3 py-1.5 border rounded"
                          />
                          <button type="button" onClick={handleClose} className="px-4 py-2 bg-red-500 text-white rounded hover:bg-red-600">Cancel</button>
                          <button type="submit" className="ml-2 px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600">
                              {modalMode === 'add' ? 'Add Event' : 'Update Event'}
                          </button>
                      </form>
                  </Box>
            </Modal>

          <section className="bg-white rounded-2xl shadow-lg m-9">
            <div className="px-6 py-4 flex justify-between items-center border-b">
              <h2 className="text-xl font-semibold">CMO Event Dates</h2>
                      
                      {/* Search Filter */}
                      <div className="flex justify-end items-center">

                      <div className="flex items-center mr-4">
                        <FontAwesomeIcon icon={faSearch} className="text-gray-400 mr-2" />
                        <TextField 
                          id="search-input"
                          className="mr-4"
                          label="Search"
                          variant="outlined"
                          size="small"
                          value={searchKeyword}
                          onChange={handleSearch}
                        />
                      </div >

                      <div>
                        {/* Date Range Filter */}
                        <DateRangePicker
                          style={{ marginRight: 10 }}
                          onChange={setDateRange}
                          placeholder="Select Date Range"
                          ranges={[{
                            label: 'Today',
                            value: [new Date(), new Date()]
                          }, {
                            label: 'Last 7 Days',
                            value: [new Date(new Date().setDate(new Date().getDate() - 6)), new Date()]
                          }, {
                            label: 'Last 30 Days',
                            value: [new Date(new Date().setDate(new Date().getDate() - 29)), new Date()]
                          }, {
                            label: 'This Month',
                            value: [new Date(new Date().getFullYear(), new Date().getMonth(), 1), new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)]
                          }, {
                            label: 'This Year',
                            value: [new Date(new Date().getFullYear(), 0, 1), new Date(new Date().getFullYear(), 11, 31)]
                          }]}
                          placement="auto"
                        />
                      </div>

                    </div>
                  </div>

                    {/* CMO Event Dates Table */}
                    <div style={{ height: 400, width: '100%' }}>
                      {isLoading?                                  
                        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '25vh', backgroundColor: 'rgba(255, 255, 255, 0.5)' }}>
                        <CircularProgress style={{ color: 'blue' }} />
                        </div> :

                            <DataGrid
                            rows={rows}
                            columns={columns}
                            pagination
                            paginationMode="server"
                            paginationModel={paginationModel}
                            onPaginationModelChange={(model) => setPaginationModel(model)}
                            rowCount={data?.totalCount || 0}
                            pageSizeOptions={[10, 20, 100]}
                            loading={isLoading}
                            />
                      }
 
                    </div>
            <div className="overflow-auto" style={{ maxHeight: 'calc(100vh - 250px)' }}>

            </div>
          </section>
        </div>
    </>
 );
};


export default Dashboard;