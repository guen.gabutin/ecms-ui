import React from 'react';
import ReactDOM from 'react-dom/client';
import { MailDataProvider } from './components/contexts/MailDataContext';
import './index.css';
import App from './App';
import { BrowserRouter } from 'react-router-dom';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'

const queryClient = new QueryClient()

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <QueryClientProvider client ={queryClient}>
    <MailDataProvider>
    <App />
    </MailDataProvider>
    <ReactQueryDevtools />
    </QueryClientProvider>
  </BrowserRouter>
);
