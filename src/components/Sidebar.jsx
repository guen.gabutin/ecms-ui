import React, {useEffect} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faTachometerAlt,
    faUser,
    faFolder,
    faFilePen,
    faCertificate,
    faCheckCircle,
    faBars,
    faSignOutAlt
} from '@fortawesome/free-solid-svg-icons';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useMailData } from './contexts/MailDataContext';

const Sidebar = ({ isCollapsed, toggleSidebar }) => {
    const navigate = useNavigate();
    const location = useLocation();
    const { username, userRole, logout } = useMailData();

    // Log the path change
    // useEffect(() => {
    //     console.log("Navigated to:", location.pathname);
    // }, [location.pathname]); // Dependency array includes location.pathname

    const handleGenerateCertificateClick = () => {
        sessionStorage.removeItem('generateVisited');
        navigate('/generate-certificate');
    };

    const handleLogout = () => {
        Swal.fire({
            title: 'Are you sure you want to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, logout',
            cancelButtonText: 'No, cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                logout();
                Swal.fire({
                    icon: 'success',
                    title: 'Logged out successfully!',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });
    
    };

    const Divider = () => (
        <hr className={`my-2 border-t mr-4 ${isCollapsed ? 'border-white' : 'hidden'} mx-auto`} style={{ width: '60%' }} />
    );

    return (
        <div className={`text-white transition-width duration-300 ease-in-out ${isCollapsed ? 'w-16' : 'w-64'} h-full fixed inset-y-0 left-0 z-30`} style={{ backgroundColor: '#3a53a5' }}>
            <div className="flex items-center justify-start p-4">
                <FontAwesomeIcon icon={faBars} onClick={toggleSidebar} size="lg" className="cursor-pointer" />
                {!isCollapsed && <span className="ml-4 text-xl font-bold">Menu</span>}
            </div>
            
            <nav className="mt-4">
                {['Admin', 'Staff', 'StudentAssistant'].includes(userRole) && (
                    <>
                        <p className={!isCollapsed ? "px-4 font-bold" : "hidden"}>MAIN</p>
                        <Link to="/dashboard" className={`flex items-center p-4 hover:bg-[#2a3a85] transition duration-200 ${location.pathname === '/dashboard'}`}>
                            <FontAwesomeIcon icon={faTachometerAlt} size="lg" className="mr-4 text-white"/>
                            {!isCollapsed && <span className='text-white'>Dashboard</span>}
                        </Link>
                        <Divider />
                    </>
                )}

                {['Admin', 'Staff', 'StudentAssistant'].includes(userRole) && (
                    <>
                        <p className={!isCollapsed ? "px-4 font-bold" : "hidden"}>DATA LIST</p>
                        {userRole === 'Admin' && (
                            <Link to="/accounts-center" className={`flex items-center p-4 hover:bg-[#2a3a85] transition duration-200 ${location.pathname === '/accounts-center'}`}>
                                <FontAwesomeIcon icon={faUser} size="lg" className="mr-4 text-white"/>
                                {!isCollapsed && <span className='text-white'>Manage Accounts</span>}
                            </Link>
                        )}
                        <Link to="/student-records" className={`flex items-center p-4 hover:bg-[#2a3a85] transition duration-200 ${location.pathname === '/student-records'}`}>
                            <FontAwesomeIcon icon={faFolder} size="lg" className="mr-4 text-white"/>
                            {!isCollapsed && <span className='text-white'>Student Records</span>}
                        </Link>
                        <Divider />
                    </>
                )}

                {['Admin', 'Staff'].includes(userRole) && (
                    <>
                        <p className={!isCollapsed ? "px-4 font-bold" : "hidden"}>CERTIFICATES</p>
                        <Link to="/certificate-templates" className={`flex items-center p-4 hover:bg-[#2a3a85] transition duration-200 ${location.pathname === '/certificate-templates'}`}>
                            <FontAwesomeIcon icon={faFilePen} size="lg" className="mr-4 text-white"/>
                            {!isCollapsed && <span className='text-white'>Certificate Templates</span>}
                        </Link>
                        <div onClick={handleGenerateCertificateClick} className={`cursor-pointer flex items-center p-4 hover:bg-[#2a3a85] transition duration-200 ${location.pathname.includes('/generate-certificate') ? 'bg-[#2a3a85]' : ''}`}>
                            <FontAwesomeIcon icon={faCertificate} size="lg" className="mr-4 text-white"/>
                            {!isCollapsed && <span className='text-white'>Generate Certificate</span>}
                        </div>
                    </>
                )}

                {['Admin', 'Staff', 'StudentAssistant'].includes(userRole) && (
                    <Link to="/certificate-scan" className={`flex items-center p-4 hover:bg-[#2a3a85] transition duration-200 ${location.pathname === '/certificate-scan'}`}>
                        <FontAwesomeIcon icon={faCheckCircle} size="lg" className="mr-4 text-white"/>
                        {!isCollapsed && <span className='text-white'>Verify Certificate</span>}
                    </Link>
                )}
            </nav>
            
            <div className="absolute bottom-0 w-full">
                {/* User details display */}
                    <div className={`p-4 text-sm ${isCollapsed ? 'hidden' : 'block'}`}>
                    <p className='font-semibold'>Welcome, {username}!</p>
                    <p className='font-semibold'>SYSTEM ROLE: {userRole}</p>
                </div>
                <button onClick={handleLogout} className="flex items-center p-4 w-full text-left hover:bg-[#2a3a85] transition duration-200" aria-label="Logout">
                    <FontAwesomeIcon icon={faSignOutAlt} size="lg" className="mr-4" />
                    {!isCollapsed && <span>Logout</span>}
                </button>
            </div>
        </div>
    );
};

export default Sidebar;
