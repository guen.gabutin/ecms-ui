import React, { createContext, useContext, useState, useEffect } from 'react';

const MailDataContext = createContext();

const departmentDetails = {
    Agriculture: {
      name: 'Agriculture',
      departments: [
        'BS Agribusiness', 
        'BS Agriculture', 
        'BS Agricultural & Biosystems Engineering', 
        'BS Food Technology', 
        'BS Development Communication'
        ],
    },
    ArtsScience: {
        name: 'Arts Science',
        departments: [
          'AB Economics',
          'AB History', 
          'AB Interdisciplinary Studies', 
          'AB International Studies', 
          'AB English Language', 
          'AB Literature', 
          'AB Philosophy',
          'AB Psychology',
          'AB Sociology', 
          'BS Biology',
          'BS Chemistry', 
          'BS Marine Biology',
          'BS Mathematics',
          'BS Psychology'
          ],
      },
    ComputerStudies: {
      name: 'Computer Studies',
      departments: [
          'BS Computer Science', 
          'BS Information Systems', 
          'BS Information Technology', 
          'BS Entertainment & Multimedia Computing', 
          ],
    },
    Engineering: {
      name: 'Engineering',
      departments: [
          'BS Chemical Engineering', 
          'BS Civil Engineerring', 
          'BS Electrical Engineering', 
          'BS Electronics Engineering', 
          'BS Industrial Engineering',
          'BS Mechanical Engineering'
          ],
    },
    Nursing: {
      name: 'Nursing',
      departments: [
          'BS Nursing',  
          ],
    },
    BusinessManagement: {
      name: 'Business Management',
      departments: [
          'BS Accountancy', 
          'BS Business Administration', 
          'BS Management Accounting'
          ],
    },
    Education: {
      name: 'Education',
      departments: [
          'Bachelor of Early Childhood Education', 
          'Bachelor of Elementary Education', 
          'Bachelor of Special Needs Education', 
          'Bachelor of Technology and Livelihood Education', 
          'Bachelor of Secondary Education'
          ],
    },
  
  };

  const majorDetails = {
    MajorsList: {
      name: 'Undergraduate',
      groups: [
        {
          label: 'Other',
          majors: ['N/A']
        },
        {
          label: 'Agriculture',
          majors: ['Animal Science (AGRI)', 'Crop Science (AGRI)']
        },
        {
          label: 'Interdisciplinary Studies',
          majors: ['Humanities (IS)', 'Social Sciences (IS)']
        },
        {
          label: 'English Language',
          majors: [
            'English Language Across the Professions (EL)',
            'English Language Studies (EL)'
          ]
        },
        {
          label: 'Literature',
          majors: [
            'Literary and Cultural Studies (LIT)',
            'Literature Across the Professions (LIT)'
          ]
        },
        {
          label: 'Philosophy',
          majors: ['Pre Divinity (PHILO)', 'Pre Law (PHILO)']
        },
        {
          label: 'Sociology',
          majors: [
            'Advancement in Leadership (SOC)',
            'Development Studies (SOC)',
            'General (SOC)'
          ]
        },
        {
            label: 'Entertainment & Multimedia Computing',
            majors: ['Digital Animation Technology (EMC)']
          },
        {
          label: 'Education',
          majors: [
            'Generalist (Special Needs Ed)',
            'Early Childhood Education (Special Needs Ed)',
            'Home Economics (TLE)',
            'English (Secondary Ed)',
            'Filipino (Secondary Ed)',
            'Mathematics (Secondary Ed)',
            'Science (Secondary Ed)',
            'Social Study (Secondary Ed)',
            'Values Education (Secondary Ed)',
            'Diploma/Profession Education (Secondary Ed)'
          ]
        },
      ]
    }
  };


export const MailDataProvider = ({ children }) => {
    const [mailData, setMailData] = useState([]);
    const [selectedTemplateId, setSelectedTemplateId] = useState('');
    const [activeTab, setActiveTab] = useState('generate');
    const [batchId, setBatchId] = useState('');

    const [selectedCollege, setSelectedCollege] = useState('');
    const [selectedDepartment, setSelectedDepartment] = useState('');
    const [selectedMajor, setSelectedMajor] = useState('');

    const [processCompleted, setProcessCompleted] = useState(false); // Added state for process completion
    const [isResetting, setIsResetting] = useState(false);

    // Initialize role from localStorage
    const [username, setUsername] = useState('');
    const [userRole, setUserRole] = useState('');

    useEffect(() => {
      // Load text from localStorage on component mount
      const storedText = localStorage.getItem('username');
      if (storedText) {
        setUsername(storedText);
      }
    }, []);

    // Update role in both context and localStorage
    const updateUserRole = (role) => {
      setUserRole(role);
    };

    const updateUserName = (username) => {
      setUsername(username);
      localStorage.setItem('username', username); 
    };

    const logout = () => {
        localStorage.removeItem('token');
        localStorage.removeItem('userRole')
        localStorage.removeItem('username')
        setUserRole(null);
        setUsername(null)
        // Redirect or enforce a full application refresh
        window.location = '/login';
    };

    // console.log(activeTab)

    const updateBatchId = (id) => {
        setBatchId(id);
    };

    const navigateToMail = () => setActiveTab('mail');
    const selectTemplate = (templateId) => {
        setSelectedTemplateId(templateId);
        // console.log("Template selected:", templateId);
    };

        // Function to reset all context data to initial values
        const resetContext = () => {
            setMailData([]);
            setSelectedTemplateId('');
            setActiveTab('generate');
            setBatchId('');
            setProcessCompleted(false); // Reset this part of the context too
            setTimeout(() => setIsResetting(false), 0); // Reset the flag after current execution context
        };

        const resetBatchId = () => {
            setBatchId(null);
        }

    return (
        <MailDataContext.Provider value={{
            mailData, setMailData,
            activeTab, setActiveTab,
            navigateToMail,
            selectTemplate,
            selectedTemplateId,
            batchId, updateBatchId,
            resetBatchId,
            processCompleted, setProcessCompleted,
            resetContext,
            departmentDetails, majorDetails,
            selectedCollege,
            setSelectedCollege,
            selectedDepartment,
            setSelectedDepartment,
            selectedMajor,
            setSelectedMajor,
            userRole,
            updateUserRole,
            username,
            updateUserName,
            logout,
            isResetting
        }}>
            {children}
        </MailDataContext.Provider>
    );
};

export const useMailData = () => useContext(MailDataContext);

export default MailDataContext;