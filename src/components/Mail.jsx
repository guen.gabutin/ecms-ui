import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useMailData } from './contexts/MailDataContext';
import axiosConfig from '../../axiosConfig';
import { useQuery } from '@tanstack/react-query';

import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';

import Swal from 'sweetalert2';


const Mail = () => {
    const { mailData, selectTemplate, activeTab, batchId, resetContext} = useMailData();
    const [searchKeyword, setSearchKeyword] = useState('');
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isDetailsModalOpen, setIsDetailsModalOpen] = useState(false);
    const [selectedTemplateId, setSelectedTemplateId] = useState('');
    const [currentTemplate, setCurrentTemplate] = useState(null);
    const [isGenerating, setIsGenerating] = useState(false);


    const navigate = useNavigate();

    // Function to handle rollback
    const handleRollback = async () => {
        // console.log('Mail Identifier:', activeTab)
        if (!batchId || isGenerating) return;
        try {
            const token = localStorage.getItem('token');
            const headers = {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            };

            await axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/generated-certificates/rollback-students', { batchId }, headers);
            // console.log("Rollback successful");
        } catch (error) {
            console.error("Rollback failed:", error);
        }
    };

    // Effect to attach and detach the event listener
    useEffect(() => {
        const handleBeforeUnload = async (event) => {
            if (isGenerating) {
                event.preventDefault();
                event.returnValue = '';  // Standard for some browsers
                await handleRollback();
            }
        };
    
        if (isGenerating) {
            window.addEventListener("beforeunload", handleBeforeUnload);
        }
    
        return () => {
            window.removeEventListener("beforeunload", handleBeforeUnload);
        };
    }, [batchId, isGenerating]);

    // Fetching templates using react-query
    const { data: templates, isLoading, error } = useQuery({
        queryKey: ['templates'],
        queryFn: fetchTemplates
    });

    // Function to fetch templates
    async function fetchTemplates() {
        const token = localStorage.getItem('token');
        const headers = {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        };
        const response = await axiosConfig.get('https://e-cms-capstone-backend-hazel.vercel.app/certificate-templates/templates-list', headers);
        return response.data.data;  // Adjust based on your API response structure
    }

    if (isLoading) return <div>Loading...</div>;
    if (error) return <div>An error occurred: {error.message}</div>;

    const filteredTemplates = templates.filter(template =>
        template.templateTitle.toLowerCase().includes(searchKeyword.toLowerCase())
    );

    const handleOpenModal = () => {
        setIsModalOpen(true);
    };

    const handleGenerate = async () => {
        if (!selectedTemplateId || !batchId) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Please select a template and ensure a batch is selected.',
            });
            return;
        }
    
        const token = localStorage.getItem('token'); // Retrieve the token from local storage
        if (!token) {
            Swal.fire({
                icon: 'warning',
                title: 'Authentication Required',
                text: 'No authentication token found. Please log in.',
            });
            return;
        }
    
        setIsGenerating(true); // Set generating to true at the start of the generation process
    
        // Display the loading Swal immediately upon button click
        Swal.fire({
            title: 'Processing...',
            text: 'Initiating certificates generation...',
            icon: 'info',
            allowOutsideClick: false,
            showConfirmButton: false,  
            didOpen: () => {
                Swal.showLoading();
    
                // Continue with the async operation
                performCertificateGeneration(token).then(() => {
                    // If the operation succeeds, the success notification will be handled by the setTimeout below
                }).catch((error) => {
                    console.error('Error generating certificates:', error);
                    Swal.update({
                        icon: 'error',
                        title: 'Failed to generate certificates',
                        text: 'Please check the console for more details.',
                        showConfirmButton: true
                    });
                    setIsGenerating(false); // Set generating to false on error
                });
    
                // Trigger the success notification after 3 seconds regardless of the operation status
                setTimeout(() => {
                    Swal.update({
                        icon: 'success',
                        title: 'Success!',
                        text: 'Certificates generated! Emails will be sent out shortly.',
                        showConfirmButton: true, // Change to true to allow manual close
                    });
    
                    // Logic to reset context or any other cleanup
                    resetContext();
                    setIsGenerating(false); // Set generating to false after the Swal update or navigation
                }, 3000);
            }
        });
    };
    
    const performCertificateGeneration = async (token) => {
        try {
            const response = await axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/generated-certificates/generate-issue-certificates', {
                batchId: batchId,
                templateId: selectedTemplateId
            }, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            return response.data;
        } catch (error) {
            console.error('Failed to generate certificates', error);
            throw error;  
        }
    };


    // Function to close all modals
    function handleCloseAll() {
        setIsModalOpen(false);
        setIsDetailsModalOpen(false);
    }

    // Function to close only the details modal and reopen the templates modal
    function handleCloseDetails() {
        setIsDetailsModalOpen(false);
        setIsModalOpen(true); // Reopen the templates modal
    }

    const handleTemplateClick = (template) => {
        setCurrentTemplate(template);
        setIsDetailsModalOpen(true);
    };

    const handleSelectTemplate = () => {
        selectTemplate(currentTemplate._id);
        setSelectedTemplateId(currentTemplate._id);
        setIsDetailsModalOpen(false);
        setIsModalOpen(false);
    };

    const modalStyle = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const renderDetailsModalContent = () => {
        if (!currentTemplate) {
            // Return a simple loading or placeholder box if no template is currently selected
            return (
                <Box sx={modalStyle}>
                    <p>Loading template details...</p>
                </Box>
            );
        }
    
        return (
            <Box sx={modalStyle}>
                <h2 id="modal-modal-title" className="text-lg font-bold mb-4">
                    View Template: {currentTemplate.templateTitle}
                </h2>
                <div className="space-y-4">
                    <p><strong>Title:</strong> {currentTemplate.templateTitle}</p>
                    <p><strong>Background Image Link:</strong> {currentTemplate.certBgImgKey}</p>
                    <p><strong>Event Year Level:</strong> {currentTemplate.certEventYearLevel}</p>
                    <p><strong>Event Type:</strong> {currentTemplate.certEventType}</p>
                    <p><strong>Event Theme:</strong> {currentTemplate.certEventTheme}</p>
                    <p><strong>Event Date:</strong> {currentTemplate.certEventDate}</p>
                    <p><strong>Event Venue:</strong> {currentTemplate.certEventVenue}</p>
                    <p><strong>Director Name:</strong> {currentTemplate.certDirectorName}</p>
                    <p><strong>Signature Image Link:</strong> {currentTemplate.certSigImgKey}</p>
                    <Button onClick={handleSelectTemplate} variant="contained" color="primary">
                        Select Template
                    </Button>
                </div>
            </Box>
        );
    };

    const columns = [
        { field: 'certificateId', headerName: 'Certificate ID', width: 200 },
        { field: 'firstName', headerName: 'First Name', width: 150 },
        { field: 'lastName', headerName: 'Last Name', width: 150 },
        { field: 'email', headerName: 'Email', width: 250 },
    ];

    return (
        <div className="min-h-full p-5">
        <div className="bg-white p-5 rounded-lg shadow-md flex justify-between items-center">
            <TextField
                label="Selected Template ID"
                variant="outlined"
                value={selectedTemplateId}
                InputProps={{
                    readOnly: true,
                }}
                style={{ marginRight: 20 }}
            />
            <Button variant="contained" onClick={handleOpenModal}>
                Select Template
            </Button>
        </div>
        <Modal
            open={isModalOpen}
            onClose={handleCloseAll} // Use handleCloseAll here to close everything if this modal is closed
            aria-labelledby="modal-modal-title"
        >
            <Box sx={{ ...modalStyle, width: 'auto', maxWidth: '90%', height: '80vh', overflow: 'hidden' }}>
                {/* Fixed Search Bar */}
                <TextField
                    label="Search Templates"
                    variant="outlined"
                    value={searchKeyword}
                    onChange={e => setSearchKeyword(e.target.value)}
                    style={{ marginBottom: 20, width: '100%' }}
                />
                {/* Scrollable Container for Templates */}
                <div style={{ height: 'calc(80vh - 60px)', overflowY: 'auto' }}> {/* Adjust height to account for TextField height */}
                    <div className="grid grid-cols-3 gap-5 p-5">
                        {filteredTemplates.map((template) => (
                            <div
                                key={template._id}
                                style={{ backgroundImage: `url(${template.certBgImgKey})`, backgroundSize: 'cover' }}
                                className="group border border-blue-600 rounded-md p-20 relative bg-white hover:bg-blue-50 transition duration-300"
                                onClick={() => handleTemplateClick(template)}
                            >
                                <div
                                    style={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        right: 0,
                                        bottom: 0,
                                        backgroundColor: 'rgba(255, 255, 255, 0.5)',
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        color: 'black'
                                    }}
                                >
                                    <h3 className="p-4 font-bold text-sm">{template.templateTitle}</h3>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </Box>
        </Modal>
        <Modal
            open={isDetailsModalOpen}
            onClose={handleCloseDetails} // Use a new handler that only closes this modal and opens the search modal
            aria-labelledby="view-template-modal-title"
        >
            {renderDetailsModalContent()}
        </Modal>
            <div style={{ height: 400, width: '100%', marginTop: 20 }}>
                <DataGrid
                    rows={mailData}
                    columns={columns}
                    pageSize={5}
                    disableSelectionOnClick
                    getRowId={(row) => row.certificateId}
                />
            </div>
            <div className='grid mt-5 justify-items-end'>
                <Button variant="contained" onClick={handleGenerate}>
                    Generate Certificates
                </Button>
            </div>
        </div>
    );
};

export default Mail;
