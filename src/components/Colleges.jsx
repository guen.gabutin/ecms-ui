import React from 'react';
import { useNavigate } from 'react-router-dom';

const CollegeCategoryCard = ({ name, bgImage, onClick }) => {
    return (
        <div className={`relative flex flex-col items-center justify-center h-40 w-full text-white text-xl font-semibold rounded shadow-lg m-2 cursor-pointer hover:bg-opacity-90 transition ease-in-out duration-150 overflow-hidden`} onClick={onClick}>
            <div className="absolute top-0 left-0 w-full h-full flex justify-center items-center p-2 pb-8">
                <img src={bgImage} alt={name} className="max-w-full max-h-full" />
            </div>
            <div className="z-10 text-black flex flex-col items-center justify-center absolute bottom-0 w-full">
                <div className="">{name}</div>
            </div>
        </div>
    );
};

const Colleges = () => {
  const navigate = useNavigate();

  const collegeCategories = [
      { name: 'AGRICULTURE', bgImage: 'assets/collegeSeals/aggies.png', path: '/student-records/Agriculture' },
      { name: 'ARTS & SCIENCES', bgImage: 'assets/collegeSeals/artsci.jpg', path: '/student-records/ArtsScience' },
      { name: 'BUSINESS MANAGEMENT', bgImage: 'assets/collegeSeals/sbm.jpg', path: '/student-records/BusinessManagement' },
      { name: 'COMPUTER STUDIES',  bgImage: 'assets/collegeSeals/compstud.png', path: '/student-records/ComputerStudies' },
      { name: 'EDUCATION',  bgImage: 'assets/collegeSeals/educ.png', path: '/student-records/Education' },
      { name: 'ENGINEERING',  bgImage: 'assets/collegeSeals/eng.png', path: '/student-records/Engineering' },
      { name: 'NURSING',  bgImage: 'assets/collegeSeals/nursing.png', path: '/student-records/Nursing' },
  ];

  const handleClick = (path) => {
      navigate(path);
  };
    
  return (
      <>
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 text-center">
              {collegeCategories.map((category, index) => (
                  <CollegeCategoryCard 
                      name={category.name} 
                      key={index} 
                      bgImage={category.bgImage} 
                      onClick={() => handleClick(category.path)}
                  />
              ))}
          </div>
      </>
  );
};

export default Colleges;