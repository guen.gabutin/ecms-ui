import React, { useEffect, useState } from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import axiosConfig from '../../axiosConfig';
import { useMailData } from './contexts/MailDataContext';

import CircularProgress from '@mui/material/CircularProgress';

const ProtectedRoute = ({ allowedRoles }) => {
    const { updateUserRole } = useMailData();
    const [isVerified, setIsVerified] = useState(false);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const verifyToken = async () => {
            const token = localStorage.getItem('token');
            // console.log('Attempting token verification');
            if (!token) {
                // console.log('No token found, redirecting to login');
                setIsLoading(false);
                return;
            }

            try {
                const response = await axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/users/verify-token', { token });
                // console.log('Verification response:', response.data);
                const { role, valid } = response.data;

                if (valid && allowedRoles.includes(role)) {
                    // console.log('Token and role verified, access granted');
                    updateUserRole(role)
                    setIsVerified(true);
                } else if (valid) {
                    // console.log('Token valid but role not permitted, redirecting to dashboard');
                    setIsVerified(false);
                    // Redirect to dashboard if user is logged in but not authorized for the route
                    setTimeout(() => {
                        window.location.href = '/dashboard'; 
                    }, 0);
                    return;
                } else {
                    // console.log('Token invalid, redirecting to login');
                    setIsVerified(false);
                }
            } catch (error) {
                console.error('Token verification failed:', error);
                setIsVerified(false);
            }
            setIsLoading(false);
        };

        verifyToken();
    }, [allowedRoles, updateUserRole]);

    if (isLoading) {
        return <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '80vh', backgroundColor: 'rgba(255, 255, 255, 0.5)' }}>
        <CircularProgress style={{ color: 'blue' }} />
        </div>; // Revised Loading Indicator
    }

    if (!isVerified) {
        return <Navigate to="/login" replace />;
    }

    return <Outlet />;
};

export default ProtectedRoute;