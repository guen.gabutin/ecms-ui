import React, { useState } from 'react';
import { FaUser, FaUsers } from 'react-icons/fa';
import { useMailData } from '../components/contexts/MailDataContext';

const Generate = () => {
  
  const { setActiveTab } = useMailData();

  return (
    <>
      <div className="text-center mb-10">
        <h2 className="text-2xl mb-4">GENERATION MODE</h2>
      </div>
      <div className="flex justify-center items-center">
        <div className="mx-6">
          <button
            onClick={() => setActiveTab('solo')}
            className="bg-blue-500 text-white p-6 rounded-lg shadow-lg text-center w-56 h-56 flex flex-col items-center justify-center"
          >
            <FaUser size={48} className="mb-4" />
            <p className="text-xl">SOLO</p>
          </button>
        </div>
        <span className="mx-4 text-gray-600 text-xl">OR</span>
        <div className="mx-6">
          <button
            onClick={() => setActiveTab('batch')}
            className="bg-blue-500 text-white p-6 rounded-lg shadow-lg text-center w-56 h-56 flex flex-col items-center justify-center"
          >
            <FaUsers size={48} className="mb-4" />
            <p className="text-xl">BATCH</p>
          </button>
        </div>
      </div>
    </>
  );
};

export default Generate;