import React, {useState, useEffect} from 'react';
import { useLocation } from 'react-router-dom';

import { useMutation } from '@tanstack/react-query';
import { useMailData } from './contexts/MailDataContext';
import axiosConfig from '../../axiosConfig'

import { FaArrowLeft } from 'react-icons/fa'; // Import an arrow icon

import { DataGrid } from '@mui/x-data-grid';

import Swal from 'sweetalert2';

// A helper function to format the department keys for display
function formatDisplayName(key) {
    // Insert a space before all caps and trim any leading space
    return key.replace(/([A-Z])/g, ' $1').trim();
}

const Solo = ( ) => {

    const { username, majorDetails, setMailData, navigateToMail, updateBatchId, resetBatchId, resetContext } = useMailData();

    const [selectedCollegeKey, setSelectedCollegeKey] = useState('');
    const [batchId, setBatchId] = useState(null);

    const { 
        departmentDetails,
     } = useMailData();

     const handleCollegeChange = (event) => {
        const key = event.target.value; // this is the actual key, not formatted
        setSelectedCollegeKey(key);  // Store the key directly
        // Reset department when college changes
        setStudentDetails(prev => ({
            ...prev,
            college: key, 
            department: ''
        }));
    };

      const [studentDetails, setStudentDetails] = useState({
        studentId: '',
        firstName: '',
        lastName: '',
        college: '',
        department: '',
        major: '',
        yearStanding: '',
        email: ''
    });

    // Initialize rows state for DataGrid
    const [rows, setRows] = useState([]);

    // useEffect(() => {
    //     // Automatically set yearStanding to empty when major is Graduate
    //     if (studentDetails.major === "Graduate") {
    //         setStudentDetails(prev => ({
    //             ...prev,
    //             yearStanding: ''
    //         }));
    //     }
    // }, [studentDetails.major]);


    const handleInputChange = (e) => {
        const { name, value } = e.target;
        // Only update the necessary field to prevent unnecessary re-renders
        setStudentDetails(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const { mutate, isLoading } = useMutation({
        mutationFn: (studentData) => {
            // Format the college name before sending the data
            const requestData = {
                ...studentData,
                college: formatDisplayName(studentData.college)
            };
            return axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/generated-certificates/upload-student', requestData, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            });
        },
        onSuccess: (data) => {
            console.log('Received data:', data);  
            const newBatchId = data.data.batchId;  
            console.log('New Batch ID:', newBatchId);  
    
            // Use requestData in response handling if necessary, else use data from the server
            setRows(prevRows => [...prevRows, { ...studentDetails, college: formatDisplayName(studentDetails.college), id: data.data.student.studentId }]);
            setBatchId(newBatchId);  
            updateBatchId(newBatchId);  
            
            Swal.fire('Success', 'Upload successful!', 'success');
        },
        onError: (error) => {
            Swal.fire('Error', `Upload failed: ${error.message}`, 'error');
        }
    });

    const validateFormData = () => {
        const { studentId, firstName, lastName, college, department, major, yearStanding, email } = studentDetails;
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        const studentIdRegex = /^\d+$/; // Regex to check if the string contains only digits

        // Conditional check for yearStanding based on the major
        if (!studentId || !firstName || !lastName || !college || !department || !major || !yearStanding || !email) {
            Swal.fire('Warning', 'Please fill out all fields.', 'warning');
            return false;
        }

        // Validate studentId to ensure it contains only numbers
        if (!studentIdRegex.test(studentId)) {
            Swal.fire('Error', 'Student ID must contain only numbers.', 'error');
            return false;
        }

        if (!emailRegex.test(email)) {
            Swal.fire('Warning', 'Please enter a valid email address.', 'warning');
            return false;
        }
    
        return true;
    };
    
    const handleUpload = () => {
        if (validateFormData()) {
            mutate(studentDetails);
        }
    };

    const assignCertificateID = useMutation({
        mutationFn: async () => {
            return axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/generated-certificates/assign-certificate-ids', { 
                batchId,
                createdBy: username 
            }, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            });
        },
        onSuccess: (data) => {
            console.log('Certificates assigned, response data:', data);
            setMailData(data.data);
            navigateToMail();
            Swal.fire({
                title: 'Success!',
                text: 'Certificates have been assigned successfully.',
                icon: 'success',
                confirmButtonText: 'OK'
            });
        },
        onError: (error) => {
            console.error('Error assigning certificates:', error);
            Swal.fire({
                title: 'Error!',
                text: 'Failed to assign certificates.',
                icon: 'error',
                confirmButtonText: 'OK'
            });
        }
    });

        // Function to handle rollback
        const handleRollback = async () => {
            if (!batchId) return;
            try {
                // Retrieve the token from local storage or another state management solution
                const token = localStorage.getItem('token');
                const headers = {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                };
        
                await axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/generated-certificates/rollback-students', { batchId }, headers);
                // console.log("Refresh Rollback successful");
                resetBatchId();
            } catch (error) {
                console.error("Rollback failed:", error);
            }
        };

        // Effect to attach and detach the event listener
        useEffect(() => {
            const handleBeforeUnload = async (event) => {
                event.preventDefault();
                await handleRollback();
            };
    
            window.addEventListener("beforeunload", handleBeforeUnload);
    
            return () => {
                window.removeEventListener("beforeunload", handleBeforeUnload);
            };
        }, [batchId]); // Depend on batchId to reattach if it changes

        // Mutation for rolling back the student batch on navigation
        const rollbackMutation = useMutation({
            mutationFn: () => axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/generated-certificates/rollback-students', { batchId }, {
                headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
            }),
            onSuccess: () => {
                console.log('Rollback successful');
                resetBatchId();
            },
            onError: (error) => {
                console.error('Rollback failed:', error);
            }
        });
    
        const handleBack = () => {
            if (batchId) {
                // Confirm rollback before navigating
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Do you want to rollback the uploaded data?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, rollback it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        rollbackMutation.mutate(null, {
                            onSuccess: () => {
                                // console.log("Rollback successful, navigating back.");
                                resetContext();
                            },
                            onError: () => {
                                console.error("Rollback failed, still navigating back.");
                                resetContext();
                                
                            }
                        });
                    } else {
                        // console.log("Rollback not confirmed, navigating back.");
                        resetContext();
                        
                    }
                });
            } else {
                // console.log("No rollback needed, navigating back.");
                resetContext();
                
            }
        };

    const columns = [
        { field: 'studentId', headerName: 'Student ID', width: 150 },
        { field: 'firstName', headerName: 'First Name', width: 150 },
        { field: 'lastName', headerName: 'Last Name', width: 150 },
        { field: 'email', headerName: 'Email', width: 250 }
    ];


  return (
    <>
               <div className="flex text-center mb-4">
                <button onClick={handleBack} className="text-blue-500 hover:text-blue-700 p-2 rounded-lg">
                    <FaArrowLeft size={20} />
                </button>
                <h2 className="text-2xl flex-grow text-center">SOLO GENERATION</h2>
            </div>
            <div className="grid grid-cols-3 gap-4 mb-8">
                <input type="text" placeholder="STUDENT ID NO." name="studentId" value={studentDetails.studentId} onChange={handleInputChange} className="border-2 rounded-lg p-3" />
                <input type="text" placeholder="FIRST NAME" name="firstName" value={studentDetails.firstName} onChange={handleInputChange} className="border-2 rounded-lg p-3" />
                <input type="text" placeholder="LAST NAME" name="lastName" value={studentDetails.lastName} onChange={handleInputChange} className="border-2 rounded-lg p-3" />
                <select name="college" value={studentDetails.college} onChange={handleCollegeChange} className="border-2 rounded-lg p-3">
                    <option value="">Select College</option>
                    {Object.keys(departmentDetails).map(key => (
                        <option key={key} value={key}>{formatDisplayName(key)}</option>
                    ))}
                </select>
                <select name="department" value={studentDetails.department} onChange={handleInputChange} disabled={!studentDetails.college} className="border-2 rounded-lg p-3">
                    <option value="">Select Department</option>
                    {studentDetails.college && departmentDetails[selectedCollegeKey].departments.map(dept => (
                        <option key={dept} value={dept}>{dept}</option>
                    ))}
                </select>
                <select name="major" value={studentDetails.major} onChange={handleInputChange} className="border-2 rounded-lg p-3">
                <option value="">Select Major</option>
                {majorDetails.MajorsList.groups.map(group => (
                        <optgroup key={group.label} label={group.label}>
                            {group.majors.map(major => (
                                <option key={major} value={major}>{major}</option>
                            ))}
                        </optgroup>
                    ))}
                </select>  
                <select name="yearStanding" value={studentDetails.yearStanding} onChange={handleInputChange} className="border-2 rounded-lg p-3" disabled={studentDetails.major === "Graduate"}>
                    <option value="">Select Year Level</option>
                    <option value="1">1st</option>
                    <option value="2">2nd</option>
                    <option value="3">3rd</option>
                    <option value="4">4th</option>
                </select>
                <input type="text" placeholder="XU EMAIL" name="email" value={studentDetails.email} onChange={handleInputChange} className="border-2 rounded-lg p-3" />
            </div>
            <div className="text-left">
                <button onClick={handleUpload} disabled={isLoading || batchId !== null} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">
                    {isLoading ? 'Uploading...' : 'Upload'}
                </button>
            </div>
            <div style={{ height: 400, width: '100%' }} className="my-4">
                <DataGrid
                    rows={rows}
                    columns={columns}
                    pageSize={5}
                    disableSelectionOnClick
                />
            </div>
            <div className="text-right">
            <button
                onClick={() => assignCertificateID.mutate()}
                disabled={!batchId} // Disable the button if no batchId is available
                className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mt-2"
            >
                Assign Certificate IDs
            </button>
            </div>
    </>
  );
};

export default Solo;