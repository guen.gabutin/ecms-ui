import { useEffect, useRef } from 'react';
import { useLocation } from 'react-router-dom';
import { useMailData } from '../contexts/MailDataContext';
import axiosConfig from '../../../axiosConfig';

const useRollback = () => {
    const { isResetting, resetBatchId } = useMailData();

    const handleRollback = async (batchId, processCompleted) => {
        if (!batchId || processCompleted || isResetting) return;

        try {
            const token = localStorage.getItem('token');
            await axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/generated-certificates/rollback-students', { batchId }, {
                headers: { 'Authorization': `Bearer ${token}` }
            });
            console.log("Rollback successful");
            resetBatchId();
        } catch (error) {
            console.error("Rollback failed:", error);
        }
    };

    return handleRollback;
};

export default useRollback;