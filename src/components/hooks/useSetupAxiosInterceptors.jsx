import { useEffect, useState } from 'react';
import axiosClient from '../../../axiosConfig';
import { useMailData } from '../contexts/MailDataContext';
import Swal from 'sweetalert2';

const useSetupAxiosInterceptors = () => {
    const { logout } = useMailData();
    const [isTokenExpired, setIsTokenExpired] = useState(false); // State to control repeated alerts

    useEffect(() => {
        const handleResponseError = (error) => {
            if (error.response && error.response.status === 401 && error.response.data.error === 'Token expired') {
                // Check state to prevent repeated dialogs
                if (!isTokenExpired) {
                    setIsTokenExpired(true); // Set the flag to true to prevent further dialogs
                    Swal.fire({
                        icon: 'error',
                        title: 'Session Expired',
                        text: 'Your login session has expired. Please log in again.',
                        confirmButtonText: 'Login'
                    }).then(result => {
                        if (result.isConfirmed) {
                            logout(); // Call the context's logout function
                        }
                    });
                }
                return new Promise(() => {}); // Prevent further processing of the error
            }
            return Promise.reject(error);
        };

        const interceptor = axiosClient.interceptors.response.use(response => response, handleResponseError);

        return () => {
            axiosClient.interceptors.response.eject(interceptor);
        };
    }, [logout, isTokenExpired]); // Include isTokenExpired to control the effect execution

    return null; // Since it's a setup function, we don't need to return anything
};

export default useSetupAxiosInterceptors;