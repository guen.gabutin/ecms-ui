import React, { useState, useEffect } from 'react';
import { useMailData } from './contexts/MailDataContext';
import { useNavigate, useLocation } from 'react-router-dom';
import { FaArrowLeft } from 'react-icons/fa';
import { DataGrid } from '@mui/x-data-grid';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import axiosConfig from '../../axiosConfig';
import Swal from 'sweetalert2';


const Batch = ( ) => {


    const { username, setMailData, navigateToMail, updateBatchId, resetBatchId, resetContext } = useMailData();
    const queryClient = useQueryClient();

    const [file, setFile] = useState(null);
    const [studentState, setStudentState] = useState([]);
    const [batchId, setBatchId] = useState(null);


    const uploadMutation = useMutation({
        mutationFn: async (fileData) => {
            Swal.fire({
                title: 'Uploading...',
                text: 'Please wait while the file is being uploaded.',
                allowOutsideClick: false,
                willOpen: () => {
                    Swal.showLoading();
                }
            });
    
            const formData = new FormData();
            formData.append('file', fileData);
            return axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/generated-certificates/upload-students', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            });
        },
        onSuccess: (data) => {
            Swal.fire({
                title: 'Success!',
                text: 'File uploaded successfully and data processed.',
                icon: 'success',
                confirmButtonText: 'OK'
            });
    
            const remappedStudents = data.data.studentsData.map(student => ({
                ...student,
                id: student._id
            }));
            queryClient.setQueryData(['students'], remappedStudents);
            setStudentState(remappedStudents);
            setBatchId(data.data.batchIds[0]);
            updateBatchId(data.data.batchIds[0]);
        },
        onError: (error) => {
            Swal.fire({
                title: 'Error!',
                text: `Failed to upload file: ${error.message}`,
                icon: 'error',
                confirmButtonText: 'OK'
            });
        }
    });

        // Mutation for rolling back the student batch on navigation
        const rollbackMutation = useMutation({
            mutationFn: () => axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/generated-certificates/rollback-students', { batchId }, {
                headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
            }),
            onSuccess: () => {
                console.log('Rollback successful');
                resetBatchId();
            },
            onError: (error) => {
                console.error('Rollback failed:', error);
            }
        });

        // Function to handle rollback
        const handleRollback = async () => {
            // console.log('Mail Identifier:', activeTab)
            if (!batchId) return;
            try {
                // Retrieve the token from local storage or another state management solution
                const token = localStorage.getItem('token');
                const headers = {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                };
        
                await axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/generated-certificates/rollback-students', { batchId }, headers);
                // console.log("Rollback successful");
                resetBatchId();
            } catch (error) {
                console.error("Rollback failed:", error);
            }
        };

        // Effect to attach and detach the event listener
        useEffect(() => {
            const handleBeforeUnload = async (event) => {
                event.preventDefault();
                await handleRollback();
            };
    
            window.addEventListener("beforeunload", handleBeforeUnload);
    
            return () => {
                window.removeEventListener("beforeunload", handleBeforeUnload);
            };
        }, [batchId]); // Depend on batchId to reattach if it changes
    
        const handleBack = () => {
            if (batchId && file) {
                // Confirm rollback before navigating
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Do you want to rollback the uploaded data?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, rollback it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        rollbackMutation.mutate(null, {
                            onSuccess: () => {
                                // console.log("Rollback successful, navigating back.");
                                resetContext();
                            },
                            onError: () => {
                                console.error("Rollback failed, still navigating back.");
                                resetContext();
                                
                            }
                        });
                    } else {
                        // console.log("Rollback not confirmed, navigating back.");
                        resetContext();
                        
                    }
                });
            } else {
                // console.log("No rollback needed, navigating back.");
                resetContext();
                
            }
        };

        const assignCertificateIDs = useMutation({
            mutationFn: async () => {
                Swal.fire({
                    title: 'Assigning Certificates...',
                    text: 'Please wait while certificates are being assigned.',
                    allowOutsideClick: false,
                    willOpen: () => {
                        Swal.showLoading();
                    }
                });
        
                return axiosConfig.post('https://e-cms-capstone-backend-hazel.vercel.app/generated-certificates/assign-certificate-ids', { batchId, createdBy: username }, {
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                });
            },
            onSuccess: (data) => {
                Swal.fire({
                    title: 'Success!',
                    text: 'Certificates have been assigned successfully.',
                    icon: 'success',
                    confirmButtonText: 'OK'
                });
        
                console.log('About to set mail data:', data);
                setMailData(data.data);
                navigateToMail();
            },
            onError: (error) => {
                Swal.fire({
                    title: 'Error!',
                    text: `Failed to assign certificates: ${error.message}`,
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
                console.error('Error assigning certificates:', error);
            }
        });

    const columns = [
        { field: 'studentId', headerName: 'Student ID', width: 150 },
        { field: 'firstName', headerName: 'First Name', width: 150 },
        { field: 'lastName', headerName: 'Last Name', width: 150 },
        { field: 'email', headerName: 'Email', width: 250 },
    ];

    return (
        <>
            <div className="flex text-center mb-4">
                <button className="text-blue-500 hover:text-blue-700 p-2 rounded-lg" onClick={handleBack}>
                    <FaArrowLeft size={20} />
                </button>
                <h2 className="text-2xl flex-grow text-center">BATCH GENERATION</h2>
            </div>
            <div className="mb-4">
                <label htmlFor="csvUpload" className="block text-gray-700 text-sm font-bold mb-2">
                    Please upload CSV file
                </label>
                <input
                    id="csvUpload"
                    type="file"
                    onChange={(e) => setFile(e.target.files[0])}
                    className="shadow border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                />
            </div>
            <button
                onClick={() => uploadMutation.mutate(file)}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mt-2"
            >
                Upload
            </button>
            <button
                onClick={() => assignCertificateIDs.mutate()}
                className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mt-2 ml-4"
            >
                Create Certificates
            </button>
            <div style={{ height: 400, width: '100%' }} className="my-4">
                <DataGrid
                    rows={studentState}
                    columns={columns}
                    pageSize={5}
                    disableSelectionOnClick
                    getRowId={(row) => row._id}
                />
            </div>
        </>
    );
};

export default Batch;
