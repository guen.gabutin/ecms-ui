import React, { useEffect } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import './App.css';
import Swal from 'sweetalert2';

// Import PWA registration utilities
import { registerSW } from 'virtual:pwa-register';

import Home from './pages/Home';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import AccountsCenter from './pages/AccountsCenter';
import StudentRecords from './pages/studentRecords';
import StudentProfile from './pages/studentProfile';
import CertificateTemplates from './pages/certificateTemplates';
import GenerateCertificate from './pages/generateCertificate';
import CertificateScan from './pages/CertificateScan';
import ProtectedRoute from './components/ProtectedRoute';

import useSetupAxiosInterceptors from './components/hooks/useSetupAxiosInterceptors';

import 'rsuite/dist/rsuite.min.css';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
const queryClient = new QueryClient();

function App() {
  useSetupAxiosInterceptors(); // Set up axios interceptors

  useEffect(() => {
    const updateSW = registerSW({
      onNeedRefresh() {
        Swal.fire({
          title: 'New content is available!',
          text: 'Do you want to reload the page to update?',
          icon: 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, update it!'
        }).then((result) => {
          if (result.isConfirmed) {
            updateSW();
          }
        });
      },
      onOfflineReady() {
        console.log('The application can work offline.');
      },
    });
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
          <Routes>
            <Route path="/" element={<Navigate replace to="/login" />} />
            <Route path='/login' element={<Login />} />

            {/* Wrap protected routes with ProtectedRoute component */}
            {/* Dashboard accessible by Admin, Staff, and StudentAssistant */}
            <Route element={<ProtectedRoute allowedRoles={['Admin', 'Staff', 'StudentAssistant']} />}>
              <Route path='/dashboard' element={<Dashboard />} />
              <Route path='/student-records/*' element={<StudentRecords />} />
              <Route path='/students/student-profile/:studentId' element={<StudentProfile />} />
              <Route path='/certificate-scan' element={<CertificateScan />} />
            </Route>

            {/* Accounts Center accessible only by Admin */}
            <Route element={<ProtectedRoute allowedRoles={['Admin']} />}>
              <Route path='/accounts-center' element={<AccountsCenter />} />
            </Route>

            {/* Certificate Templates, Generate Certificate accessible by Admin and Staff */}
            <Route element={<ProtectedRoute allowedRoles={['Admin', 'Staff']} />}>
              <Route path='/certificate-templates' element={<CertificateTemplates />} />
              <Route path='/generate-certificate' element={<GenerateCertificate />} />
            </Route>
          </Routes>
      </div>
    </QueryClientProvider>
  );
}

export default App;
